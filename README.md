# felyx_report

## Development

### Install dependencies

- install dependencies and project

```bash
poetry install -vv
```

### Pre-commit

- register pre-commit

```bash
pre-commit install
```

- Run the hooks manually

```bash
-commit
```

### Check code quality

```bash
flake8 .
```

### Run unit tests

```bash
pytest --tb=line
```

## Setup the system configuration
```bash
export FELYX_SYS_CFG=<project_dir>/tests/resources/config_sys_felyx.yaml
```

## Launch application
```bash
usage: felyx-report [-h] -c CONFIGURATION_FILE --start START --stop STOP --manifest_dir MANIFEST_DIR [--metric_dir METRIC_DIR]
                    --mdb_dir MDB_DIR [--logfile LOGFILE] [--logfile_level LOGFILE_LEVEL] [-v | -q | -s]
                    [-P [key=value [key=value ...]]] [-C [key=value [key=value ...]]]

Produce monitoring report from data processed by Felyx processor

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIGURATION_FILE, --configuration_file CONFIGURATION_FILE
                        Configuration file path
  --start START         Time frame start date (YYYY-mm-ddTHH:MM:SS format)
  --stop STOP           Time frame end date (YYYY-mm-ddTHH:MM:SS format)
  --manifest_dir MANIFEST_DIR
                        Manifest files root dir path
  --metric_dir METRIC_DIR
                        Metric files root dir path
  --mdb_dir MDB_DIR     Matchup files root dir path
  --logfile LOGFILE     Path of the file where logs will be written
  --logfile_level LOGFILE_LEVEL
                        Minimal level that log messages must reach to be written in the log file
  -v, --verbose         Activate debug level logging - for extra feedback.
  -q, --quiet           Disable information logging - for reduced feedback.
  -s, --silence         Log ONLY the most critical or fatal errors.
  -P [key=value [key=value ...]]
                        Update of parameter items
  -C [key=value [key=value ...]]
                        Update of configuration items

```

##Example:
```bash
felyx-report -c <path_to_configuration_file>.yaml --start 2018-07-01 --stop 2018-07-31 --manifest_dir <path_to_manifest_root_dir> --mdb_dir <path_to_mdb_root_dir> --metric_dir <path_to_metrics_root_dir>
```
