from typing import List

import pytest

from felyx_report.cli.main import felyx_monitoring_report


@pytest.fixture(scope='module')
def felyx_report_args(resources_dir, felyx_sys_config_file) -> List[str]:
    return [
        '--start',
        '2018-07-01T00:00:00',
        '--end',
        '2018-07-10T23:59:59',
        '-c',
        str(resources_dir / 'felyx_report_test.yaml'),
        '--felyx-sys-cfg',
        str(felyx_sys_config_file),
        '--metric-dir',
        str(resources_dir / 'metrics'),
        '--manifest-dir',
        str(resources_dir / 'manifest'),
        '--mdb-dir',
        str(resources_dir / 'mdb'),
    ]


def test_felyx_report(felyx_report_args):
    """Test felyx report."""
    print('test_felyx_report')

    felyx_monitoring_report(felyx_report_args)

    # after generate_mmdb_reports
    #       10 mmdb_reports
    # after prepare_docs.prepare_indexes_and_datasources
    #       20 mmdb_docs
    #       Elasticsearch index created : isi_cersat_felyx_report_test_cmems-l2p-swh-jason-3__cmems_wave
    #       Grafana datasource created : isi_cersat_felyx_report_test (index name: isi_cersat_felyx_report_test_*)
    # after index_docs.process
    #       20 docs indexed in isi_cersat_felyx_report_test_cmems-l2p-swh-jason-3__cmems_wave
    # after dashboard.process
    #       1 dashboard created under root_folder (felyx_report_test) : CMEMS-L2P-SWH-Jason-3__cmems_wave
