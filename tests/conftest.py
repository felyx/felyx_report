import logging
import os
from pathlib import Path

import pandas as pd
from _pytest.fixtures import fixture
from pydantic_settings import BaseSettings

from felyx_processor.utils.configuration import FelyxProcessorConfig
from felyx_report.core.configuration import FelyxReportConfig
from felyx_report.monitoring.grafana.api_client import GrafanaAPIClient


class FelyxTestConfig(BaseSettings):
    force_reinstall_root_dir: bool = False
    logging_level: str = 'ERROR'
    create_api_key: bool = False

    class Config:  # noqa: D106, WPS431
        env_prefix = 'felyx_report_tu_'
        validate_all = True
        validate_assignment = True


def setup_logging():
    es_logger = logging.getLogger('elasticsearch')
    es_logger.setLevel(logging.WARNING)

    felyx_logger = logging.getLogger('felyx.report')
    felyx_logger.setLevel(logging.WARNING)


def setup_pandas():
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)


@fixture(scope='session')
def test_dir():
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return Path(__file__).parent


@fixture(scope='session')
def felyx_root_dir(test_dir):
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return test_dir / '_build'


@fixture(scope='session')
def felyx_sys_config_file(test_dir) -> Path:
    return test_dir / 'config_sys_felyx.yaml'


@fixture(scope='session')
def felyx_report_config(felyx_root_dir, felyx_sys_config_file) -> FelyxReportConfig:
    os.environ['FELYX_PROCESSOR_ROOT_DIR'] = str(felyx_root_dir.absolute())
    return FelyxReportConfig.from_yaml_file(felyx_sys_config_file)


@fixture(scope='session')
def felyx_test_config() -> FelyxTestConfig:
    return FelyxTestConfig()


@fixture(scope='session')
def resources_dir(test_dir) -> Path:
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return test_dir / 'resources'


@fixture(scope='session')
def felyx_processor_config(felyx_processor_config_file) -> FelyxProcessorConfig:
    return FelyxProcessorConfig.from_yaml_file(felyx_processor_config_file)


@fixture(scope='session')
def setup_grafana(felyx_test_config: FelyxTestConfig, felyx_report_config: FelyxReportConfig):
    print('*' * 70)
    if not felyx_test_config.create_api_key:
        print('Do no create API KEY')
        return
    print('create API KEY')
    grafana_api_client = GrafanaAPIClient(grafana_config=felyx_report_config.grafana)
    api_key = grafana_api_client.create_api_key(name='apiKeyCI')
    felyx_report_config.grafana.api_key = api_key
