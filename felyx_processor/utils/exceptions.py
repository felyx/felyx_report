# encoding: utf-8
"""
exceptions
--------------------------------------

Contains classes for felyx processor exceptions

"""


class FelyxProcessorError(Exception):
    """Base class for felyx processor exceptions """


class ConfigException(FelyxProcessorError):
    """Configuration exception class."""


class ElasticsearchError(FelyxProcessorError):
    """Elasticsearch client exception """


class QueryToJsonError(FelyxProcessorError):
    """Conversion quary to JSON exception """


class UnknownOperatorError(FelyxProcessorError):
    """Unknown operator exception """


class InvalidWTKPointError(FelyxProcessorError):
    """Invalid WTK point exception """


class InvalidTimespanError(FelyxProcessorError):
    """Invalid time span exception """


class ParquetFileError(FelyxProcessorError):
    """Parquet file exception """


class InsituDataError(FelyxProcessorError):
    """Insitu data exception """


class NoDatasetExtractionFound(FelyxProcessorError):
    """Insitu data exception """


class MetricProcessingError(FelyxProcessorError):
    """Metric processing exception """
    def __init__(self, name, dataset, collection, miniprod, msg, exc):
        """"""
        self.msg = msg
        self.exception = exc
        self.name = name
        self.dataset = dataset
        self.collection = collection
        self.miniprod = miniprod
