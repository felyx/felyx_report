# coding=utf-8

"""
felyx_processor.utils.arguments
------------------------------

This package provides a common framework for loading and updating
the arguments of felyx programs.

.. :copyright:
.. :license:

.. sectionauthor::
.. codeauthor::
"""

from __future__ import annotations

import argparse
import re
from pathlib import Path
from typing import Dict, List

import yaml


def parse_all_args(
    parent_parser: argparse.ArgumentParser,
    cli_args: List[str]
) -> argparse.Namespace:

    parser = argparse.ArgumentParser(
        parents=[parent_parser],
        add_help=False,
        description=parent_parser.description,
        formatter_class=argparse.RawTextHelpFormatter
    )

    args = parser.parse_args(cli_args)

    return args


def read_yaml_data(filepath: str, override_data: List[str] = None) -> dict:
    yaml_data = {}
    if filepath is not None:
        yaml_file = Path(filepath)
        if yaml_file.exists():
            with open(yaml_file) as open_file:
                yaml_data = yaml.load(open_file, Loader=yaml.SafeLoader)

    if override_data is not None:
        yaml_data = get_override_args(yaml_data, override_data)

    return yaml_data


def get_override_args(data_to_update: Dict = None, override_data: List[str] = None) -> Dict:
    if override_data is None:
        return data_to_update

    if data_to_update is None:
        data_to_update = {}

    def walk_items(items, value):
        d = dict()
        key = items.pop(0)
        if len(items) > 0:
            d[key] = walk_items(items, value)
        else:
            if value.startswith('[') and value.endswith(']'):
                # transformation of str into list
                value = [_.strip('\' \"') for _ in value[1:-1].split(',')]
            d[key] = value
        return d

    def update_data(data, override):
        for key, value in override.items():
            if key not in data:
                data[key] = value
                continue
            if not isinstance(value, dict) or not isinstance(data[key], dict):
                data[key] = value
                continue

            data[key] = update_data(data[key], value)
        return data

    for option in override_data:
        matcher = re.match(r'^([^=]*)=(.*)$', option)
        # case : invalid option
        if matcher is None:
            print(f"Invalid override property '{option}'. Must be key=value")
            continue

        data_to_update = update_data(
            data_to_update,
            walk_items(matcher.group(1).split('.'), matcher.group(2))
        )
    return data_to_update
