# coding=utf-8

"""
felyx_processor.utils.arguments
------------------------------

This package provides a common framework for setting up logging

.. :copyright:
.. :license:

.. sectionauthor::
.. codeauthor::
"""

import logging
from pathlib import Path

import felyx_processor

LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)-100s|%(filename)s:%(lineno)d@%(funcName)s()'
logger = logging.getLogger('felyx.processor')


def build_logfile_handler(
        logfile_path: Path,
        logfile_level: str
) -> logging.FileHandler:
    """

    :param logfile_path: File path
    :param logfile_level: Log level
    :return: File Handler for logging
    """
    abs_path = logfile_path.resolve()
    logdir = abs_path.parent

    ok = felyx_processor.storage.makedirs(logdir)
    if not ok:
        logger.error('Could not create parent directory for the logfile')
        return None

    file_handler = logging.FileHandler(abs_path, mode='a', encoding='utf-8',
                                       delay=True)
    if 'debug' == logfile_level:
        file_handler.setLevel(logging.DEBUG)
    elif 'info' == logfile_level:
        file_handler.setLevel(logging.INFO)
    elif 'warning' == logfile_level:
        file_handler.setLevel(logging.WARNING)
    elif 'error' == logfile_level:
        file_handler.setLevel(logging.ERROR)
    else:
        logger.error('Unknown log level "{}"'.format(logfile_level))
        return None

    return file_handler


def setup_logging(
    app_logger: logging.Logger,
    settings  # : felyx_processor.utils.parameters.LogOptions
) -> bool:
    """

    :param app_logger: Main logger
    :param settings: log paramterscat
    :return: True if setup OK
    """

    app_logger.setLevel(logging.DEBUG)
    app_logger.handlers = []
    handler = logging.StreamHandler()
    formatter = logging.Formatter(fmt=LOG_FORMAT)
    handler.setFormatter(formatter)
    app_logger.addHandler(handler)

    if settings.verbose:
        handler.setLevel(logging.DEBUG)
    elif settings.quiet:
        handler.setLevel(logging.WARNING)
    elif settings.silent:
        handler.setLevel(logging.FATAL)
    else:
        handler.setLevel(logging.INFO)

    if settings.logfile is not None:
        file_handler = build_logfile_handler(
            settings.logfile, settings.logfile_level)
        if file_handler is None:
            _msg = 'Could not create a handler to log messages to "{}"'
            logger.error(_msg.format(settings.logfile))
            return False
        file_handler.setFormatter(formatter)
        app_logger.addHandler(file_handler)

    return True
