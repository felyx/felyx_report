# encoding: utf-8
"""
es_insitu_datasource
--------------------------------------

Contains classe for retrieve In Situ data from Elastic Search

"""
import collections
import logging
import operator
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List, OrderedDict, Union

import numpy as np
import pandas as pd
from elasticsearch import ElasticsearchException
from pandas import DataFrame, Series, Timestamp
from shapely import wkt

import felyx_processor.sites.insitu_datasource as insitu_datasource
from felyx_processor.index.es.es_client import ESClient, ESSearchQuery
from felyx_processor.sites.insitu_datasource import InSituDataSource
from felyx_processor.utils.configuration import ElasticSearchConfig
from felyx_processor.utils.exceptions import ElasticsearchError, FelyxProcessorError, InvalidWTKPointError

try:
    pass
except ImportError:
    pass

# ES_OPERATIONS  : keys of the operations discribed in the ES Configuration for
# In Situ data
ES_INSITU_SEARCH = 'insitu_search'
ES_INSITU_INDEX = 'insitu_index'
ES_INSITU_DELETE = 'insitu_delete'

# RELATIVE PATH TO PAWS INDEX MAPPINGS
INDEX_RAWS_MAPPINGS_RELATIVE_PATH = 'share/es/data_mappings.json'

TIME_FMT = '%Y-%m-%d %H:%M:%S'
TIME_FMT_1 = '%Y%m%d%H%M%S'

LOG = logging.getLogger('felyx.processor')


class ESInSituDataSource(InSituDataSource):
    """Elastic Search In Situ Data Source"""

    def __init__(
            self,
            yaml_config: ElasticSearchConfig = None
    ):
        """
        Elasticsearch Data Source constructor

        Args:
            yaml_config: the YAML ES configuration
        """
        super().__init__()

        self.__config = yaml_config
        if self.__config is None:
            self.__config = ElasticSearchConfig()

    def es_position_to_list(self,
                            position: Dict
                            ) -> [float, float, datetime]:
        """
        Convert dict to a position

        Args:
            position: the dictionary containing the positions.
        Returns:
            A list with position and time.
        Raises:
            InvalidWTKPointError: If position contains an invalid WKT point.
        """
        shape = position['_source']['shape']
        end_datetime = position['_source']['time_coverage']['lte']
        match = wkt.loads(shape)
        if match is None:
            msg = 'Shape "{}" is not a valid WKT point'.format(shape)
            LOG.error(msg)
            raise InvalidWTKPointError(msg)

        lon = match.x
        lat = match.y
        # Convert time string to datetime
        dt = datetime.strptime(end_datetime, TIME_FMT)
        return [lon, lat, dt]

    def get_raws_count_by_day(self,
                              collection_code: str,
                              start: datetime,
                              stop: datetime,
                              ) -> OrderedDict[Timestamp, int]:
        """
        Search the raws number in a collection over a timespan and group them
        by day.
        Args:
            collection_code : the collection to search
            start : the begin time coverage
            stop : the end time coverage
        Returns:
            A dictionary with days as keys and raws count as values.
        Raises:
            FelyxProcessorError: If fails to extract raws count.
        """

        # Init ES client
        clientES = ESClient(self.__config)
        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())

        # Create the list of the days to compute
        time_range = pd.date_range(start=start, end=stop, freq='D')
        nb_raws = dict.fromkeys(time_range, 0)
        nb_raws = collections.OrderedDict(sorted(nb_raws.items()))

        # For each day, retrieve the number of raws from ES
        for day in nb_raws.keys():
            start = day
            stop = day + timedelta(seconds=24 * 3600 - 1)
            try:
                nb_raws[day] = clientES.get_docs_count(
                    index_path=index_path,
                    time_field='time_coverage',
                    start=start, stop=stop)
            except FelyxProcessorError:
                msg = 'Failed to retrieve raws count for collection {}'.format(
                    collection_code)
                raise FelyxProcessorError(msg)

        if sum(nb_raws.values()) == 0:
            LOG.info(
                'No raws found for collection {} and time range : {} {}'.format(
                    collection_code, start, stop))

        return nb_raws

    def get_trajectories(self,
                         collection_code: str,
                         sites_codes: List[str],
                         start: datetime,
                         stop: datetime,
                         as_dataframe: bool = True
                         ) -> Union[Dict, DataFrame]:
        """
        Search trajectories in a collection and list of sites over a timespan
        Args:
            collection_code : the collection to search
            sites_codes : the list of sites codes to search
            start : the begin time coverage
            stop : the end time coverage
            as_dataframe : if True, returns a dataframe,
            if False, returns a dictionary.
        Returns:
            if as_dataframe is set to True :
                a pandas dataframe with columns : time, site, lat, lon
            else :
                a dictionary containing the trajectories with keys equal to
                site code and values equal to a list of lat, lon, date
        Raises:
            FelyxProcessorError: If fails to extract trajectories.
        """
        trajectories = dict()

        # Create search query
        terms_filters = dict()
        if sites_codes is not None:
            terms_filters['site'] = sites_codes

        range_filters = dict()
        if None not in (start, stop):
            range_filters['time_coverage'] = (start, stop)

        query = ESSearchQuery(['site', 'shape', 'time_coverage.lte'],
                              terms_filters, range_filters, None, None, True)
        clientES = ESClient(self.__config)

        # Get the positions from ES client
        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())

        try:
            results = clientES.search(index_path, query, ES_INSITU_SEARCH)
        except FelyxProcessorError:
            msg = 'Failed to retrieve positions for collection {}'.format(
                collection_code)
            if sites_codes is not None:
                LOG.error(
                    'Request was limited to the following sites: {}'.format(
                        ', '.join(sites_codes)))
            raise FelyxProcessorError(msg)

        # Construct the trajectories
        for position in results:
            site_code = position['_source']['site']
            p = self.es_position_to_list(position)
            if site_code not in trajectories:
                trajectories[site_code] = [p]
            else:
                trajectories[site_code].append(p)

        # Reorganize data
        for site_code in trajectories:
            trajectories[site_code].sort(key=operator.itemgetter(2))

        if as_dataframe is True:
            # Convert the trajectory dict to dataframe
            trajectories_df = self.convert_trajectories_to_dataframe(
                trajectories, collection_code)
            return trajectories_df

        return trajectories

    def _from_raws_response(self,
                            es_response: List[Dict],
                            measurements: List[str]
                            ) -> Dict:
        """
        Convert the ES response to dict.
        Args
            es_response: the ES raws results
            measurements: the parameter list to keep
        Returns
            A converted dictionary.
        """
        # Metrics are tied to an extraction, i.e. a (dataset, collection)
        # couple, and several collections can share the same products.
        # There is one document per extraction in the ES metrics index, meaning
        # that the metrics associated with one miniprod are potentially spread
        # across several ES documents.
        # In order to regroup all the metrics associated with a miniprod, it
        #  is necessary to loop over all results, and merge the results which
        # have the same dataset, site and start datetime (these fields are
        # enough to uniquely identify one miniprod in most cases).
        tmp = dict()
        _available_metrics = dict()
        for hit in es_response:
            dataset = hit['_source']['dataset']
            site = hit['_source']['site']
            start_str = hit['_source']['time_coverage']['gte']
            if dataset not in tmp:
                tmp[dataset] = dict()
            if site not in tmp[dataset]:
                tmp[dataset][site] = dict()
            if start_str not in tmp[dataset][site]:
                tmp[dataset][site][start_str] = dict()

            metric_results = hit['_source']['data']
            for metric_data in metric_results:
                metric_name = metric_data['name']

                _available_metrics[metric_name] = True

                # Only keep requested metrics
                if (measurements is not None) and (
                        metric_name not in measurements):
                    continue

                # Extract metric value
                metric_value = None
                if 'txt_value' in metric_data:
                    metric_value = metric_data['txt_value']
                elif 'num_value' in metric_data:
                    metric_value = metric_data['num_value']
                elif 'int_value' in metric_data:
                    metric_value = metric_data['int_value']
                elif 'bool_value' in metric_data:
                    metric_value = metric_data['bool_value']
                elif 'no_value' in metric_data:
                    metric_value = None  # same as default value

                # Store metric
                tmp[dataset][site][start_str][metric_name] = metric_value

            _shape = hit['_source']['shape']
            coords = _shape[_shape.rfind('(') + 1:_shape.find(')')].split(' ')
            lon, lat = [float(_) for _ in coords]
            tmp[dataset][site][start_str]['Longitude'] = lon
            tmp[dataset][site][start_str]['Latitude'] = lat
            tmp[dataset][site][start_str]['Time'] = start_str
            _available_metrics['Longitude'] = True
            _available_metrics['Latitude'] = True
            _available_metrics['Time'] = True

        if measurements is None:
            measurements = list(_available_metrics.keys())

        # Reformat the results and use placeholders (None) when one of the
        #  requested metrics is not available.
        # For each site there is a dictioanry of metrics. Each metric is
        # associated with a list of values. The size of these lists *must*
        # match the number of items in the "times" list, i.e. the *must* be one
        # value per metric and per miniprod extracted for each site.
        result = dict()
        for dataset in tmp:
            result[dataset] = dict()
            for site in tmp[dataset]:
                result[dataset][site] = {'times': [], 'metrics': {}}

                for metric_name in measurements:
                    result[dataset][site]['metrics'][metric_name] = []

                for start_str in tmp[dataset][site]:
                    _start = start_str.replace(' ', '').replace('-', '')
                    _start = _start.replace(':', '')

                    tmp_metrics = tmp[dataset][site][start_str]
                    for metric_name in measurements:
                        value = None
                        if metric_name in tmp_metrics:
                            value = tmp_metrics[metric_name]
                        result[dataset][site]['metrics'][metric_name].append(
                            value)

                    result[dataset][site]['times'].append(start_str)

        return result

    def get_raws(self,
                 collection_code: str,
                 sites_codes: List[str],
                 start: datetime,
                 stop: datetime,
                 wkt_coords: List[List[float]] = None,
                 constraints_list: List[Dict[str, str]] = None,
                 measurements: List[str] = None,
                 as_dataframe: bool = True
                 ) -> Union[Dict, DataFrame]:
        """
            Search In situ data in a collection and list of sites over a
            timespan and spatial filter.

            Args:
                collection_code : the collection to search
                sites_codes : the list of sites codes to search
                start : the begin time coverage
                stop : the end time coverage
                wkt_coords (optional) : the spatial filter defined by a list of
                coordinates of a polygon
                constraints_list (optional) : additional data filters
                measurements (optional) : list of physical parameters to extract
                as_dataframe : if True, returns a dataframe, if False, returns
                a dictionary.
            Returns:
                if as_dataframe is set to True :
                    a pandas dataframe with following columns :
                    time, site, param1, param2, ...,paramN
                else :
                    a output dict formatted as following :
                        {'collection_code':
                            {'site_code':
                                {'times': ['YYYY-MM-DD HH:MM:SS'],
                                'metrics': {'measurement_1': [float_value],
                                'measurement_2': [float_value], ...
                                'Longitude': [float_value],
                                'Latitude': [float_value],
                                'Time': ['YYYY-MM-DD HH::MM:SS']}
                                }
                            }
                        }

            Raises:
                FelyxProcessorError: If fails to retrieve raws
        """

        # Create search query
        terms_filters = dict()
        if sites_codes is not None:
            terms_filters['site'] = sites_codes

        datasets = [collection_code]
        if datasets is not None:
            terms_filters['dataset'] = datasets

        range_filters = dict()
        range_filters['time_coverage'] = (start, stop)

        fields = ['site', 'dataset', 'time_coverage', 'shape', 'data.name',
                  'data.no_value', 'data.num_value', 'data.int_value',
                  'data.bool_value', 'data.txt_value']

        query = ESSearchQuery(fields, terms_filters, range_filters, wkt_coords,
                              constraints_list)
        clientES = ESClient(self.__config)

        # Get the data from ES
        index_path = '{0}raw_{1}'.format(self.__config.prefix,
                                         collection_code.lower())

        try:
            response = clientES.search(index_path, query, ES_INSITU_SEARCH)
        except FelyxProcessorError:
            msg = 'Failed to retrieve raws data for collection {}'.format(
                collection_code)
            LOG.error(msg)
            if sites_codes is not None:
                LOG.error(
                    'Request was limited to the following sites: {}'.format(
                        ', '.join(sites_codes)))
            raise ElasticsearchError(msg)

        # Extract data from response
        output = self._from_raws_response(response, measurements)

        if as_dataframe is True:
            # Convert the raws dict to dataframe
            raws_df = self.convert_raws_to_dataframe(output, collection_code)
            return raws_df

        return output

    def convert_raws_to_dataframe(
            self,
            raws: Dict,
            collection_code: str,
    ) -> DataFrame:
        """
            Convert raws dict to pandas dataframe
            Args:
                raws: the dictionary to convert
                collection_code: the collection to convert
            Returns:
                a pandas dataframe with columns : Time, site, param1, param2 ...
            """

        raws_df = DataFrame()

        if len(raws) > 0:
            # For each site
            for site in raws[collection_code]:
                dataframe = DataFrame(raws[collection_code][site]['metrics'])

                # Convert strings to datetime64
                times = list(map(lambda x: np.datetime64(x),
                                 raws[collection_code][site]['times']))
                dataframe[insitu_datasource.TIME_COLUMN] = times
                dataframe[insitu_datasource.SITE_COLUMN] = site
                dataframe.sort_values(inplace=True,
                                      by=[insitu_datasource.TIME_COLUMN])
                raws_df = pd.concat([raws_df, dataframe], ignore_index=True)

            # Add the collection code to dataframe metadata
            raws_df._metadata = ['collection']
            raws_df.collection = collection_code

            raws_df = raws_df.set_index(['id', 'time'])

        return raws_df

    def convert_trajectories_to_dataframe(
            self,
            trajectories: Dict,
            collection_code: str,
    ) -> DataFrame:
        """
        Convert raws dict to pandas dataframe
        Args:
            trajectories: a dict containing the trajectories with keys equal
             to site codes and values equal to a list of lat, lon, date
            collection_code: the collection to convert
        Returns:
            a pandas dataframe with columns : Time, site, Latitude, Longitude
        """

        traj_df = DataFrame()
        # For each site
        for site in trajectories:
            dataframe = DataFrame(trajectories[site],
                                  columns=[insitu_datasource.LONGITUDE_COLUMN,
                                           insitu_datasource.LATITUDE_COLUMN,
                                           insitu_datasource.TIME_COLUMN])
            dataframe[insitu_datasource.SITE_COLUMN] = site
            # Convert strings to datetime64
            times = list(map(lambda x: np.datetime64(x),
                             dataframe[insitu_datasource.TIME_COLUMN].values))
            dataframe[insitu_datasource.TIME_COLUMN] = times
            traj_df = pd.concat([traj_df, dataframe], ignore_index=True)

        # Add the collection code to dataframe metadata
        traj_df._metadata = ['collection']
        traj_df.collection = collection_code

        return traj_df

    def get_sites_list(
            self,
            collection_code: str,
            start: datetime = datetime(1950, 1, 1),
            stop: datetime = datetime.now()
    ) -> List[str]:
        """
        Retrieve the site list of a collection
        Args:
            collection_code : the name of the collection
            start (optional) : the begin time coverage
            stop (optional) : the end time coverage
        Returns:
            a the site list (names)

        Raises:
            ElasticsearchError: If failed to retrieve sites from ES.
        """

        # Get the site names from ES client
        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())
        site_names = list()
        try:
            clientES = ESClient(self.__config)
            fieldname = 'site'
            for result in clientES.iterate_distinct_field(
                    fieldname=fieldname, index_path=index_path,
                    operation_key=ES_INSITU_SEARCH, ):
                site_names.append(result['key'][fieldname])
        except ElasticsearchException as e:
            msg = 'Elasticsearch search failure : {} ' \
                  'failed to retrieve sites for collections {}'.\
                format(e, collection_code)
            LOG.error(msg)
            raise ElasticsearchError(msg)

        return site_names

    def compute_time_coverage(self,
                              date_time: datetime
                              ) -> Dict:
        """
            Compute time coverage ES field
            Args:
                date_time: the datetime to compute
            Returns:
                A dictionary with time coverage values.
        """
        time_coverage = dict()
        time_coverage['gte'] = date_time.strftime(TIME_FMT)
        time_coverage['lte'] = date_time.strftime(TIME_FMT)
        return time_coverage

    def convert_raw_data_row_to_dict(self,
                                     raw_data: Series,
                                     collection_code: str) -> Dict:
        """
            Compute raw data ES field
            Args:
                raw_data: the raw data to convert
                collection_code: the collection name
            Returns:
                A dictionary with the raw data values.
        """
        raw_dict = dict()

        # Compute time data
        raw_dict['time_coverage'] = self.compute_time_coverage(
            raw_data[insitu_datasource.TIME_COLUMN])

        # Compute shape data
        raw_dict['shape'] = 'POINT({} {})'.format(
            raw_data[insitu_datasource.LONGITUDE_COLUMN],
            raw_data[insitu_datasource.LATITUDE_COLUMN])

        # Add collection, site, dataset
        # For insitu data, dataset and collection are the same
        raw_dict['site'] = raw_data[insitu_datasource.SITE_COLUMN]
        raw_dict['collection'] = collection_code
        raw_dict['dataset'] = collection_code

        # For each raw column data
        raw_dict['data'] = list()
        fields_list = list()
        for idx, value in raw_data.items():
            if idx not in insitu_datasource.LOCATION_COLUMNS:
                fields = dict()
                # Adapt field name to value type
                if value is None:
                    key = 'no_value'
                    value = True
                elif isinstance(value, bool):
                    key = 'bool_value'
                elif isinstance(value, str):
                    key = 'txt_value'
                elif isinstance(value, int):
                    key = 'int_value'
                else:
                    key = 'num_value'
                fields['name'] = idx
                fields[key] = value
                fields_list.append(fields)

        raw_dict['data'] = fields_list

        return raw_dict

    def delete_site_raws(self,
                         collection_code: str,
                         site_code: str,
                         start_time: datetime,
                         end_time: datetime) -> int:
        """
            Delete insitu raws data matching a site, collection in a time range
            Args:
                collection_code: the collection name
                site_code: the site name
                start_time: the time range start
                end_time: the time range end
            Returns:
                The number of deleted documents
        """

        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())
        clientES = ESClient(self.__config)

        # Retrieve in the index the documents matching the site and the
        # timeframe
        es_raws_ids = self.get_raws_ids(collection_code, site_code,
                                        start=start_time,
                                        stop=end_time)
        nb_docs = 0
        if len(es_raws_ids) > 0:
            # Remove the documents from the ES index
            nb_docs = clientES.delete_by_ids(index_path,
                                             es_raws_ids,
                                             ES_INSITU_DELETE)

            LOG.debug('{} documents removed for site : {}'.format(nb_docs,
                                                                  site_code))
        return nb_docs

    def register_raws(self,
                      collection_code: str,
                      site_code: str,
                      raws_df: DataFrame,
                      clean: bool = True
                      ):
        """
            Register insitu raws data
            Args:
              collection_code: the collection name to register
              site_code: the site name to register
              raws_df: the dataframe containing the raws data
              clean: If True, delete previous records that overlap the
              timeframe of the update
            Raises:
                FelyxProcessorError: If failed to register raws.
        """

        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())
        clientES = ESClient(self.__config)

        if 0 >= len(raws_df):
            LOG.warning('Cannot register raws from an empty dataframe ...')
            return

        raws_df.sort_values(inplace=True, by=[insitu_datasource.SITE_COLUMN,
                                              insitu_datasource.TIME_COLUMN],
                            ignore_index=True)

        # Delete previous records that overlap the timeframe of the update
        if clean is True:
            # Retrieve the time frame of the raws data
            start_time = raws_df[insitu_datasource.TIME_COLUMN].min()
            end_time = raws_df[insitu_datasource.TIME_COLUMN].max()

            nb_deleted_docs = self.delete_site_raws(
                                    collection_code=collection_code,
                                    site_code=site_code,
                                    start_time=start_time,
                                    end_time=end_time)

            if nb_deleted_docs > 0 and nb_deleted_docs != len(raws_df):
                LOG.warning('{} documents removed but {} documents to index '
                            'for site : {}'
                            .format(nb_deleted_docs, len(raws_df), site_code))

        # For insitu data, dataset and collection are the same
        dataset = collection_code

        # Compute the id of each observation of the dataframe
        raws_df['_id'] = raws_df.apply(
            lambda x: '{}_{}_{}_{}'.format(
                collection_code, dataset,
                x[insitu_datasource.SITE_COLUMN],
                (x[insitu_datasource.TIME_COLUMN]).strftime(TIME_FMT_1)),
            axis=1)

        raws_subdf = raws_df.set_index(['_id'])
        raws_subdf.replace(to_replace={np.nan: None}, inplace=True)

        # Convert each raw data to dict
        raws_subdf['data'] = raws_subdf.apply(
            lambda x: self.convert_raw_data_row_to_dict(
                x, collection_code=collection_code), axis=1)

        docs = list()
        for index, row in raws_subdf.iterrows():
            docs.append((index, row['data']))

        # Index  documents in Elastic search
        try:
            response = clientES.index(index_path, docs, ES_INSITU_INDEX)
            LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to register positions')
            raise e

    def create_raws_index(self,
                          collection_code: str
                          ):
        """
            Create the raw collection index
            Args:
                collection_code: the collection name
            Raises:
                FelyxProcessorError : If failed to create index.
        """
        # Create raw collection index in Elastic search
        clientES = ESClient(self.__config)
        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())

        mappings_path = Path(__file__).parent.parent / \
                        INDEX_RAWS_MAPPINGS_RELATIVE_PATH

        try:
            # Check if the index already exists, if not, create it
            if not clientES.has_index(index_path):
                response = clientES.create_index(index_path,
                                                 mappings_path)
                LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to create raws index : {}'.format(index_path))
            raise e

    def delete_raws_index(self,
                          collection_code: str
                          ):
        """
            Delete the raw collection index
            Args:
                collection_code: the collection name
            Raises:
                FelyxProcessorError: If failed to delete index.
        """
        # Delete raw collection index in Elastic search
        clientES = ESClient(self.__config)
        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())
        try:
            # Check if the index exists before deleting
            if clientES.has_index(index_path):
                response = clientES.delete_index(index_path)
                LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to delete raws index : {}'.format(index_path))
            raise e

    def get_raws_ids(self,
                     collection_code: str,
                     site_code: str,
                     start: datetime,
                     stop: datetime
                     ) -> List[str]:
        """
            Get raws ids for a site, collection and time range
            Args:
                collection_code : the collection to search
                site_code : the site code to search
                start : the begin time coverage
                stop : the end time coverage
            Returns:
                The list of raws ids
            Raises:
                FelyxProcessorError: If failed to retrieve raws ids.
        """
        # Create search query
        terms_filters = dict()
        terms_filters['site'] = [site_code]
        range_filters = dict()
        range_filters['time_coverage'] = (start, stop)
        fields = []

        query = ESSearchQuery(fields, terms_filters, range_filters)

        index_path = '{}raw_{}'.format(self.__config.prefix,
                                       collection_code.lower())
        clientES = ESClient(self.__config)

        try:
            response = clientES.search(index_path, query, ES_INSITU_SEARCH,
                                       clear_scroll=True)
            raws_ids = list(map(lambda x: x['_id'], response))

        except FelyxProcessorError as e:
            LOG.error('Failed to retrieve raws ids')
            LOG.debug('Request was limited to collection : {} site : {} '
                      'time start : {} end : {}'
                      .format(collection_code, site_code, start, stop))
            raise e

        return raws_ids
