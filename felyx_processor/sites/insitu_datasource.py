# encoding: utf-8
"""
insitu_datasource
--------------------------------------

Contains base classe for retrieve In Situ data from different sources

"""
import abc
import datetime
import logging
from typing import Dict, List, OrderedDict, Union

import numpy as np
import pandas as pd
import xarray as xr
from pandas import DataFrame, Timestamp

LOG = logging.getLogger('felyx.processor')

# COLUMNS MAPPING
TIME_COLUMN = 'time'
SITE_COLUMN = 'id'
LATITUDE_COLUMN = 'lat'
LONGITUDE_COLUMN = 'lon'
Z_AXIS = 'z'

# required columns for location data
LOCATION_COLUMNS = [SITE_COLUMN, TIME_COLUMN, LATITUDE_COLUMN, LONGITUDE_COLUMN]

DEFAULT_FILLVALS = {
    np.dtype('object'): '\x00',
    np.dtype('str'): '\x00',
    np.dtype('int8'): -127,
    np.dtype('uint8'): 255,
    np.dtype('int16'): -32767,
    np.dtype('uint16'): 65535,
    np.dtype('int32'): -2147483647,
    np.dtype('uint32'): 4294967295,
    np.dtype('int64'): -9223372036854775806,
    np.dtype('uint64'): 18446744073709551614,
    np.dtype('float32'): 1e+20,
    np.dtype('float64'): 1e+20,
}


class InSituDataSource(metaclass=abc.ABCMeta):
    """In Situ Data Source Interface"""

    @abc.abstractmethod
    def get_trajectories(self,
                         collection_code: str,
                         sites_codes: List[str],
                         start: datetime,
                         stop: datetime,
                         as_dataframe: bool = True
                         ) -> Union[Dict, DataFrame]:
        """
        Search trajectories in a collection and list of sites over a timespan
        Args:
            collection_code : the collection to search
            sites_codes : the list of sites codes to search
            start : the begin time coverage
            stop : the end time coverage
            as_dataframe : if True, returns a dataframe,
                            if False, returns a dictionary.
        Returns:
            if as_dataframe is set to True :
                a pandas dataframe with columns : time, site, lat, lon
            else :
                a dict containing the trajectories with key equals to site code
                and value equal to a list of lat, lon, date
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_raws(self,
                 collection_code: str,
                 sites_codes: List[str],
                 start: datetime,
                 stop: datetime,
                 wkt_coords: List[List[float]] = None,
                 constraints_list: List[Dict[str, str]] = None,
                 measurements: List[str] = None,
                 as_dataframe: bool = True
                 ) -> Union[Dict, DataFrame]:
        """
        Search In situ data in a collection and list of sites over a timespan
        and spatial filter.
        Args:
            collection_code : the collection to search
            sites_codes : the list of sites codes to search
            start : the begin time coverage
            stop : the end time coverage
            wkt_coords (optional) : the spatial filter defined by a list of
            coordinates of a polygon
            constraints_list (optional) : additional data filters
            measurements (optional) : list of physical parameters to extract
            as_dataframe : if True, returns a dataframe, if False, returns a dict
        Returns:
            if as_dataframe is set to True :
                a pandas dataframe with following columns :
                Time, site, param1, param2, ...,paramN
            else :
                a output dict formatted as following :
                    {'collection_code':
                        {'site_code':
                            {'times': ['YYYY-MM-DD HH:MM:SS'],
                            'metrics': {'measurement_1': [float_value],
                                        'measurement_2': [float_value], ...
                            'Longitude': [float_value],
                            'Latitude': [float_value],
                            'Time': ['YYYY-MM-DD HH::MM:SS']}
                            }
                        }
                    }
        """
        raise NotImplementedError

    def get_raws_in_window(
            self,
            collection_code: str,
            sites_codes: List[str],
            times: List[datetime.datetime],
            time_window: datetime.timedelta,
            wkt_coords=None,
            constraints_list: List[str] = None,
            measurements: List[str] = None,
            prefix: str = '__site',
            as_dataset: bool = False
    ) -> Union[pd.DataFrame, xr.Dataset]:
        """
        Returns, for a list of sites and associate measurement times, the in
        situ data within a time window around the given measurement time.

        Args:
            collection_code : the collection to search
            sites_codes : the list of sites codes to search
            times: the measurement time associated to each site
            time_window: the half-size of the time window (in situ data
              in [-time_window,+time_window] to be returned
            wkt_coords (optional) : the spatial filter defined by a list of
              coordinates of a polygon
            constraints_list (optional) : additional data filters
            measurements (optional) : list of physical parameters to extract
            prefix: prefix for output column names (default: ``__sites_``)
            as_dataset: return the result as a xarray Dataset if True (pandas
                dataframe if False, by default)

        Returns:
            a pandas dataframe with the following columns : __site_id,
              __site_time, __site_param1, __site_param2, ..., __site_paramN (or
              another prefix)
        """
        start = times.min()
        end = times.max()

        insitu = self.get_raws(
            collection_code,
            list(set(sites_codes)),
            start=start - time_window,
            stop=end + time_window,
            as_dataframe=True
        )

        metadata = insitu.attrs

        # match with sites
        g_insitu = insitu.groupby('id')

        def select_insitu(id, obs, data, time):
            data = data.loc[(id, time - time_window):(id, time + time_window)]
            times_values = data.index.get_level_values('time')
            u, indices = np.unique(times_values, return_inverse=True)
            data.insert(0, 'obs', obs)
            data.insert(0, 'site_obs', indices)
            return data

        m_insitu = pd.concat([
            select_insitu(_, i, g_insitu.get_group(_), times[i])
            for i, _ in enumerate(sites_codes)
        ])

        # copy metadata back
        m_insitu.attrs.update(metadata)

        # re-organize dataframe index
        # index can include optional `z` dimension
        index = ['obs', 'site_obs']
        if 'z' in m_insitu.index.names:
            index.append('z')
        m_insitu = m_insitu.reset_index().set_index(index)

        # reformat for export to another object (like xarray)
        m_insitu = self.from_pandas(m_insitu)

        m_insitu = m_insitu.rename(
            columns={v: prefix+'_'+v
                     for v in set(m_insitu.columns) - {'obs', 'site_obs'}},
            copy=False
        )

        if as_dataset:
            site_metadata = m_insitu.attrs

            # convert to xarray
            if 'z' in m_insitu.index.names:
                # ignore z dimension for id, lat, lon, time
                core = [prefix + '_' + v for v in ['id', 'lat', 'lon', 'time']]

                vars = [v for v in m_insitu.columns if v not in core]
                core_df = m_insitu[core].drop_duplicates()
                core_df = core_df.reset_index(level='z', drop=True)
                vars_ds = m_insitu[vars].to_xarray()
                m_insitu = core_df.to_xarray().merge(vars_ds)
                m_insitu = m_insitu.rename({'z': prefix + '_z'})
            else:
                m_insitu = m_insitu.to_xarray()

            # set variables attributes for site data
            self.set_site_var_attrs(
                m_insitu, site_metadata, prefix=prefix)

        return m_insitu

    @abc.abstractmethod
    def get_sites_list(
            self,
            collection_code: str,
            start: datetime = None,
            stop: datetime = None
    ) -> List[str]:
        """
        Retrieve the site list of a collection
        Args:
            collection_code : the name of the collection
            start (optional) : the begin time coverage
            stop (optional) : the end time coverage
        Returns:
            a the site list (names)
        """
        raise NotImplementedError

    @staticmethod
    def register_raws(self,
                      collection_code: str,
                      site_code: str,
                      raws_df: DataFrame,
                      clean: bool = True
                      ):
        """
            Register insitu raws data
            Args:
                collection_code: the collection name to register
                site_code: the site name to register
                raws_df: the dataframe containing the raws data
                clean: If True, delete previous records that overlap the
                timeframe of the update
        """
        raise NotImplementedError

    @staticmethod
    def from_pandas(df: pd.DataFrame, prefix: str = None) -> pd.DataFrame:
        """Transform a pandas DataFrame object into a new DataFrame with
        correct dtypes and _FillValues to be exported to an xarray or another
        object.

        Pandas uses its own dtypes and missing values (pd.NA). Conversion to
        other object types, such as xarray Dataset, will fail if they are not
        converted to more standard representation.

        Args:
            df: The pandas data frame.
            prefix: The prefix.

        Returns:
            The converted data frame.
        """
        metadata = df.attrs
        for v in df:
            c_v = v
            if prefix is not None:
                c_v = v.replace(f'{prefix}_', '')

            # true (python) dtype
            try:
                t_dtype = metadata['felyx_globals']['vars'][c_v]['dtype']
            except:
                t_dtype = df[v].dtype

            try:
                df[v] = df[v].astype(t_dtype)
            except ValueError:
                try:
                    fill_value = metadata['felyx_globals'][c_v][
                        '_FillValue']
                except KeyError:
                    fill_value = 0
                    LOG.warning(
                        f'The variable {c_v} in the dynamic site data has no '
                        f'_FillValue defined but contains missing values. felyx'
                        f' will use {fill_value} as default _FillValue but it '
                        f'is safer to define it in the metadata.')

                df[v][pd.isna(df[v])] = fill_value
                df[v] = df[v].astype(t_dtype)

        return df

    @staticmethod
    def set_site_var_attrs(
            dataset: xr.Dataset,
            insitu_metadata: Dict,
            prefix: str = '__site'):
        """Fill in the metadata of the xarray representation of a set of
        dynamic site data.

        Correct the attributes so that the Dataset can be save in netCDF
        without errors.

        Args:
            dataset: The xarray dataset.
            insitu_metadata: The dictionary of metadata.
            prefix: The prefix.
        """
        if 'felyx_globals' not in insitu_metadata:
            LOG.warning(
                'Missing metadata for dynamic site data. Output dtype '
                'and attributes may be wrong or missing.')
            return

        # global metadata
        global_attrs = insitu_metadata['felyx_globals']['globals']
        dataset.attrs.update(
            {f'{prefix}_{attr}': value for attr, value in global_attrs.items()})

        v_metadata = insitu_metadata['felyx_globals']['vars']
        for v in v_metadata:
            if v == 'id':
                continue
            prefixed_v = f'{prefix}_{v}'
            if prefixed_v not in dataset.variables:
                LOG.warning(
                    f'Variable {v} was not found in the dynamic site '
                    f'data though it is described in its metadata')
                continue

            if 'dtype' in v_metadata[v]:
                if not np.issubdtype(v_metadata[v]['dtype'], np.datetime64) \
                        and not np.issubdtype(v_metadata[v]['dtype'], np.str_):
                    dataset[prefixed_v] = dataset[prefixed_v].astype(
                        v_metadata[v]['dtype'], copy=False)
                dataset[prefixed_v].encoding['dtype'] = v_metadata[v].pop(
                    'dtype')

            if 'units' in v_metadata[v] and \
                    np.issubdtype(dataset[prefixed_v].dtype, np.datetime64):
                # ignore original time units. A common reference is used
                # everywhere.
                v_metadata[v].pop('units')

            if '_FillValue' in v_metadata[v]:
                if not np.issubdtype(dataset[prefixed_v].dtype, np.datetime64):
                    dataset[prefixed_v] = dataset[prefixed_v].fillna(
                        v_metadata[v]['_FillValue']
                    )
                dataset[prefixed_v].encoding['_FillValue'] = \
                    v_metadata[v].pop('_FillValue')

            dataset[prefixed_v].attrs.update(v_metadata[v])


    @abc.abstractmethod
    def get_raws_count_by_day(self,
                              collection_code: str,
                              start: datetime,
                              stop: datetime,
                              ) -> OrderedDict[Timestamp, int]:
        """
        Search the raws number in a collection over a timespan and group them
        by day.
        Args:
            collection_code : the collection to search
            start : the begin time coverage
            stop : the end time coverage
        Returns:
            A dictionary with days as keys and raws count as values.
        """
        raise NotImplementedError
