"""Felyx report main command.

This command is used to collect data from files previously processed or produced
by Felyx extraction and assemble processes.
The input is a Felyx processing configuration file and the output a report indexed in Elasticsearch or a json document.
"""

import argparse
import logging
import sys
from typing import List, NoReturn, Tuple

from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import get_parameters
from felyx_report.core.configuration import (
    FelyxReportConfig,
    FelyxReportProcessorConfig,
    load_felyx_report_config,
    load_felyx_report_processor_config,
)
from felyx_report.core.parameters import FelyxReportParams
from felyx_report.report.generate.mmdb_extracter import generate_mmdb_reports
from felyx_report.report.index import index_docs
from felyx_report.report.monitor import dashboard
from felyx_report.report.prepare import prepare_docs

logger = logging.getLogger('felyx.report')


def parse_args(  # noqa: WPS213
    cli_args: List[str],
) -> Tuple[FelyxReportConfig, FelyxReportProcessorConfig, FelyxReportParams]:
    """Parse the command line arguments.

    Args:
        cli_args: The command line arguments to be parsed.

    Returns:
        A tuple containing the felyx system configuration, the felyx processor configuration and the command parameters.

    Raises:
        FileNotFoundError: If the configuration file does not exist.
    """
    report_parser = argparse.ArgumentParser(
        description='Produce monitoring report from data processed by Felyx processor',
    )
    report_parser.add_argument(
        '-c',
        '--configuration_file',
        required=True,
        help='Configuration file path',
    )
    report_parser.add_argument(
        '--felyx-sys-cfg',
        required=True,
        help='System configuration file path',
    )
    report_parser.add_argument(
        '--start',
        required=True,
        help='Time frame start date (YYYY-mm-ddTHH:MM:SS format)',
    )
    report_parser.add_argument(
        '--end',
        required=True,
        help='Time frame end date (YYYY-mm-ddTHH:MM:SS format)',
        dest='stop',
    )
    report_parser.add_argument(
        '--manifest-dir',
        required=True,
        help='Manifest files root dir path',
    )
    report_parser.add_argument(
        '--metric-dir',
        default=None,
        help='Metric files root dir path',
    )
    report_parser.add_argument(
        '--mdb-dir',
        required=True,
        help='Matchup files root dir path',
    )
    report_parser.add_argument(
        '--logfile',
        default=None,
        help='Path of the file where logs will be written',
    )
    report_parser.add_argument(
        '--logfile-level',
        default=None,
        help='Minimal level that log messages must reach to be written in the log file',
    )
    level_group = report_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        help='Activate debug level logging - for extra feedback.',
    )
    level_group.add_argument(
        '-q',
        '--quiet',
        action='store_true',
        help='Disable information logging - for reduced feedback.',
    )
    level_group.add_argument(
        '-s',
        '--silence',
        action='store_true',
        help='Log ONLY the most critical or fatal errors.',
    )
    args = parse_all_args(report_parser, cli_args)

    try:
        try:
            felyx_processor_config = load_felyx_report_processor_config(
                configuration_file=args.configuration_file
            )
        except FileNotFoundError:
            raise FileNotFoundError('{0} configuration file does not exist'.format(args.configuration_file))
        try:
            felyx_sys_config = load_felyx_report_config(
                configuration_file=args.felyx_sys_cfg
            )
            felyx_report_params = get_parameters(
                params_type=FelyxReportParams,
                args=args,
                felyx_sys_config=felyx_sys_config,
            )
        except FileNotFoundError:
            raise FileNotFoundError('{0} system configuration file does not exist'.format(args.felyx_sys_cfg))

        return felyx_sys_config, felyx_processor_config, felyx_report_params

    except Exception as exception:
        logger.exception(exception)
        logger.exception('Done (failure)')
        sys.exit(1)


def exit_success() -> NoReturn:
    """Exit command with success."""
    logger.info('Done (success)')
    sys.exit(0)


def exit_failure() -> NoReturn:
    """Exit command with failure."""
    logger.error('Done (failure)')
    sys.exit(1)


def felyx_monitoring_report(cli_args=None):
    """Create felyx monitoring reports.

    Args:
        cli_args: The command line arguments
    """
    main_logger = logging.getLogger()

    # 1. Retrieve configuration and command line arguments
    felyx_sys_config, felyx_processor_config, felyx_report_params = parse_args(cli_args)
    if not setup_logging(main_logger, felyx_report_params.log):
        exit_failure()

    logger.info('Start of felyx_monitoring_report')

    # 2. Generate one MMDBReport per MMDataset in configuration file
    mmdb_reports = generate_mmdb_reports(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        felyx_report_params=felyx_report_params,
    )

    # 3. Prepare MMDB docs
    mmdb_docs = prepare_docs.prepare_indexes_and_datasources(
        mmdb_reports=mmdb_reports,
        es_config=felyx_sys_config.elasticsearch,
        grafana_config=felyx_sys_config.grafana,
    )

    # 4. Index in ES
    index_docs.process(es_config=felyx_sys_config.elasticsearch, index_dict=mmdb_docs)

    # 5. Generate new Grafana dashboards
    dashboard.process(
        mmdb_reports=mmdb_reports,
        grafana_config=felyx_sys_config.grafana,
        es_prefix=felyx_sys_config.elasticsearch.prefix,
        contact_point_configs=felyx_sys_config.contact_points,
        felyx_report_processor_config=felyx_processor_config,
    )

    logger.info('End of felyx_monitoring_report')
