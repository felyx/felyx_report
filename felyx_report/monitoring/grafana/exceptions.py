"""GrafanaApiClient exceptions."""


class GrafanaApiClientError(Exception):
    """Base class for GrafanaApiClient exceptions."""


class GrafanaHttpClientError(Exception):
    """Grafana HTTP client exception."""


class GrafanaError(GrafanaApiClientError):
    """Grafana python client exception."""
