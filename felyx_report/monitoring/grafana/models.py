"""Grafana models."""

from typing import Optional

from pydantic import BaseModel, SecretStr

from felyx_processor.utils.configuration import FelyxModel


class GrafanaCreateJsonData(FelyxModel):
    """Model to create the json data to create a Grafana Elasticsearch datasource."""

    es_version: str
    max_concurrent_shard_requests: int
    time_field: str

    def to_api_dict(self) -> dict:
        """Serialize a GrafanaCreateJsonData to Grafana API format.

        Returns:
            A Grafana API compliant dictionary representing a GrafanaCreateJsonData
        """
        return {
            'esVersion': self.es_version,
            'maxConcurrentShardRequests': self.max_concurrent_shard_requests,
            'timeField': self.time_field,
        }


class SecretModel(BaseModel):
    password: SecretStr


class GrafanaCreateElasticsearchDatasource(FelyxModel):
    """Model to create a Grafana Elasticsearch datasource."""

    name: str
    type: str
    access: str
    url: str
    password: SecretModel
    user: str
    database: str  # index es
    basic_auth: bool
    json_data: Optional[GrafanaCreateJsonData]

    def to_api_dict(self) -> dict:
        """Serialize a GrafanaCreateElasticsearchDatasource to Grafana API format.

        Returns:
            A Grafana API compliant dictionary representing a GrafanaCreateElasticsearchDatasource
        """
        return {
            'name': self.name,
            'type': self.type,
            'access': self.access,
            'url': self.url,
            'basicAuthUser': self.user,
            'database': self.database,
            'basicAuth': self.basic_auth,
            'jsonData': self.json_data.to_api_dict(),
            'secureJsonData': {
                'basicAuthPassword': self.password.password.get_secret_value(),
            },
        }


class GrafanaFolder(FelyxModel):
    """Grafana folder model."""

    id: Optional[int]
    uid: Optional[str]
    title: str
