"""Index MMDB report documents."""

from typing import Dict, List, Tuple

from felyx_processor.index.es.es_client import ESClient
from felyx_processor.utils.configuration import ElasticSearchConfig

# operation key described in ElasticSearchConfig
ES_REPORT_INDEX = 'report_index'


def process(es_config: ElasticSearchConfig, index_dict: Dict[str, List[Tuple[str, Dict]]]):  # noqa: WPS234
    """Index MMDB report documents in Elasticsearch.

    Args:
        es_config: The Elasticsearch configuration
        index_dict: The indexation dictionary
    """
    es_client = ESClient(es_config=es_config)
    for index_name, docs in index_dict.items():
        es_client.index(
            index_path=index_name,
            docs=docs,
            operation_key=ES_REPORT_INDEX,
        )
