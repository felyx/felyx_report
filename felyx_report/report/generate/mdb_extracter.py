import logging
from datetime import datetime
from pathlib import Path
from typing import List

import pandas as pd
import xarray as xr
from pandas import DataFrame

from felyx_processor.utils.configuration import MDBOutputConfig, ProductConfig
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_report.core.models import MatchUpsSection

logger = logging.getLogger('felyx.report')


def _compute_file_to_read(
    mdb_root_dir: Path,
    mdb_output_cfg: MDBOutputConfig,
    product_name: str,
    start: datetime,
    stop: datetime,
) -> List[Path]:
    """
    Compute the list of mdb product files to read from the time coverage
    Args:
        mdb_output_cfg : The mdb files output configuration.
        product_name : The product name.
        start : the begin time coverage
        stop : the end time coverage
    Returns:
        a the file names list (absolute paths)
    """

    # break down extraction period into regular intervals
    frequency = f'{mdb_output_cfg.frequency}'

    dleft = pd.date_range(start, periods=1, normalize=True, freq=frequency)
    if dleft[0] > start:
        dleft = dleft.shift(-1)
    start = dleft[0]

    dright = pd.date_range(stop, periods=1, normalize=True, freq=frequency)
    while dright[-1] < stop:
        dright = dright.shift(1)
    stop = dright[-1]

    date_list = pd.date_range(start, stop, freq=frequency)

    # Compute file names
    file_list = list(set([
        mdb_root_dir / Path(date.strftime(
            mdb_output_cfg.products[product_name].filenaming))
        for date in date_list]))

    # keep only existing files
    file_list = [_ for _ in file_list if _.exists()]
    file_list.sort()

    for file in file_list:
        logger.info('MDB file to read : {}'.format(file))

    if len(file_list) == 0:
        logger.warning(
            f'There are no matching MDB file names for product : '
            f'{product_name}')

    return file_list


def _read_mdb_file(
    mdb_product_config: ProductConfig,
    mdb_product_name: str,
    mdb_file: Path,
    start: datetime,
    stop: datetime,
) -> DataFrame:
    """
    Read the data from the mdb file belonging to the time range.
    Args:
        mdb_file : The full path of the mdb file.
        mdb_product_config : The product configuration.
        mdb_product_name : The name of the product.
        start : The time range start.
        stop : The time range end.
    Returns:
        a dataframe containing the file data with day in index and a matchups
        count column for each EO dataset.
    Raises:
        FelyxProcessorError : When mdb file reading failure.
    """

    # Read the mdb file
    with xr.open_dataset(mdb_file, engine='netcdf4') as mdb:
        try:
            # Compute the dimensions to drop and the variables to keep
            # from the prefixes list of the product config
            dim_list = list()
            col_list = list()
            for eo_datasource_name, content_cfg in \
                    mdb_product_config.content.items():
                # Check if the prefix time dimension exists in the file
                if content_cfg.prefix + '_time' in mdb.dims:
                    dim_list.append(content_cfg.prefix + '_time')
                #  :cciseastate___dataset_id =
                #  "ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM" ;
                # Check the presence of the EO dataset matching the prefix
                # in global attributes
                if content_cfg.prefix + '___dataset_id' in mdb.attrs:
                    # Add the prefix____source variable to read if present
                    if content_cfg.prefix + '___source' in mdb.data_vars:
                        col_list.append(content_cfg.prefix + '___source')
            dim_list.append('site_obs')
            col_list.append('time')

            # Drop the unwanted dimensions
            mdb = mdb.drop_dims(dim_list)
            # Keep only the time and <prefix>_source variables
            # and convert to dataframe
            mdb_df = (mdb[col_list]).to_dataframe()

            # Keep the rows in the time range
            mdb_df = mdb_df[((mdb_df['time'] >= start) &
                             (mdb_df['time'] <= stop))]

            # Count the matchups => count of the rows with non empty values
            # and group the counters by day
            mdb_df.set_index(mdb_df.time, inplace=True)
            mdb_df = mdb_df.replace({'': None}).groupby(
                pd.Grouper(freq='D')).count()

            # If prefix_source variable is absent from the mdb file,
            # create it and set it to 0
            if content_cfg.prefix + '___source' not in mdb.data_vars:
                mdb_df[content_cfg.prefix + '___source'] = 0

            # Add the product name as column
            mdb_df.insert(0, 'product', mdb_product_name)
        except IOError:
            msg = 'Error while reading assembled file : {}'.format(mdb_file)
            logger.error(msg)
            raise FelyxProcessorError(msg)

    return mdb_df


def search_matchups_infos(
    mdb_output_config: MDBOutputConfig,
    mdb_root_dir: Path,
    start: datetime,
    end: datetime,
) -> DataFrame:
    """
    Extract matchups counts from the mdb files comprised in the time range
    Args:
        mdb_output_config : The assemble output configuration.
        mdb_root_dir : The mdb root directory.
        start : The time range start.
        end : The time range end.
    Returns:
        a dataframe containing the data of the mdb files with day in index
        and a matchups count column for each EO dataset.
    Raises:
        FelyxProcessorError: When EO variable is missing in the assembled file.
    """

    matchups_df = DataFrame()
    # For each assemble product
    for product_name, product_config in mdb_output_config.products.items():
        # Compute the mdb file list to read
        mdb_files_names = _compute_file_to_read(
            mdb_root_dir=mdb_root_dir,
            mdb_output_cfg=mdb_output_config, product_name=product_name,
            start=start, stop=end)

        # Read each file and group data by each day of the time range
        for mdb_file_name in mdb_files_names:
            mdb_df = _read_mdb_file(
                mdb_product_config=mdb_output_config.products[product_name],
                mdb_product_name=product_name,
                mdb_file=mdb_file_name,
                start=start, stop=end)
            matchups_df = pd.concat([matchups_df, mdb_df])

        # print('product : {} matchups_df : {}'.format(product_name, matchups_df))
    # print('mmdb matchups_df : {}'.format(matchups_df))

    return matchups_df


def compute_matchups_section(
    matchups_df: DataFrame,
    day: datetime,
    eo_dataset_name: str,
    mdb_output_config: MDBOutputConfig,
) -> MatchUpsSection:
    """
    Compute a report matchup section from mdb dataframe for a given day and
    an given EO dataset.
    Args:
        matchups_df : The mdb dataframe containing the matchups counters.
        day : The day to compute.
        eo_dataset_name : The EO dataset name to compute.
        mdb_output_config : The assemble output configuration.
    Returns:
        A report matchup section.
    Raises:
        FelyxProcessorError: When EO variable is missing in the assembled file.
    """
    # Construct dict of counters with the products
    nb_matchups_dict = dict()
    nb_mmdb_files_dict = dict()
    for product_name, product_config in mdb_output_config.products.items():
        # Extract matchups of the day
        # print('index : {}'.format(matchups_df.index))
        day_df = matchups_df[matchups_df.index == day]

        # Column of the EO dataset if it is present in the product content
        if eo_dataset_name in product_config.content:
            eo_column = product_config.content[eo_dataset_name].prefix \
                        + '___source'
            nb_matchups = 0
            nb_mmdb_files = 0
            if eo_column in day_df.columns:
                # We keep only the lines belonging to the product :
                # A dataset can be used by several products and
                # a prefix can be used by several products
                day_df_product = day_df[day_df['product'] == product_name]
                nb_matchups = day_df_product[eo_column].sum()
                nb_mmdb_files = day_df_product[eo_column].count()
                # print('day df col : {}'.format(day_df_product[eo_column]))

            nb_matchups_dict[product_name] = nb_matchups
            nb_mmdb_files_dict[product_name] = nb_mmdb_files

    # print('-------------------- nb_mmdb_files_dict : {}'.format(nb_mmdb_files_dict))
    # print('-------------------- nb_matchups_dict : {}'.format(nb_matchups_dict))

    # Construct the matchups section
    matchups_section = MatchUpsSection(
        nb_mmdb_files=nb_mmdb_files_dict,
        nb_matchups_manifest=0,
        nb_assembled_matchups=nb_matchups_dict
    )
    return matchups_section
