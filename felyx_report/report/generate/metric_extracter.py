""" Extracter for Felyx metric data """
import collections
import glob
import json
import logging
from datetime import datetime
from pathlib import Path
from typing import Dict, List

import pandas as pd
from pandas import DataFrame, Timestamp

from felyx_processor.utils.configuration import MetricConfig
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_report.core.models import MetricsSection

logger = logging.getLogger('felyx.report')


def _get_metrics_counts(metric_file: Path,
                        metric_labels: List[str],
                        ) -> Dict[str, int]:
    """
    Compute the number of metrics per metric in the metric file.
    Args:
        metric_file : The full path of the metric file.
        metric_labels : The list of the labels of the metrics to find.
    Returns:
        a dictionary containing the number of metrics per metric name.
    Raises:
        FelyxProcessorError : When metric file reading failure.
    """
    metric_counts = dict.fromkeys(metric_labels, 0)
    try:
        with open(metric_file.resolve()) as json_file:
            data = json.load(json_file)
    except IOError:
        msg = 'Error while reading metric file : {}'.format(
            metric_file.resolve())
        logger.error(msg)
        raise FelyxProcessorError(msg)

    df = pd.json_normalize(data, 'data')
    df = df.groupby(['name']).size()
    # print('metric df : {}'.format(df))
    for key, value in df.items():
        if key in metric_counts.keys():
            metric_counts[key] = value
    return metric_counts


def search_metrics_infos(eo_dataset_name: str,
                         site_collection: str,
                         metric_configs: Dict[str, MetricConfig],
                         metric_root_dir: Path,
                         time_range: List[Timestamp],
                         ) -> DataFrame:
    """
    Compute the number of metrics
    per metric, EO datasource and for each timestamp of the time range.
    Args:
        eo_dataset_name : The name of the EO datasource.
        site_collection : The name of the Insitu collection.
        metric_configs : The dictionary of the metric configurations
        metric_root_dir : The metric root directory.
        time_range : The time range to compute.
    Returns:
        a dataframe containing :
            - A column for the EO dataset name.
            - A column for the day of the time range.
            - A column for each count of metric defined in the metric configs
    Raises:
        None
    """

    metric_nb = dict()
    metric_files = dict.fromkeys(time_range, 0)
    metric_files = collections.OrderedDict(sorted(metric_files.items()))

    # Compute the metric file path
    # /opt/felyx/data/metrics/collection/dataset_eo/site/Y/D/fichier.json
    metric_file_path = metric_root_dir / site_collection / eo_dataset_name
    metric_file_path = str(metric_file_path.resolve()) + '/*/%Y/%j/*.json'
    # print('metric file path : {}'.format(metric_file_path))

    # For each time of the time range
    for date in metric_files.keys():
        # Get the files for the time
        for file in glob.glob(date.strftime(metric_file_path)):
            # print('metric file : {}'.format(file))
            # If file exists :
            if Path(file).exists():
                # Search metrics in the file
                metric_labels = [cfg.label for cfg in metric_configs.values()]
                metric_counts = _get_metrics_counts(Path(file),
                                                   metric_labels)
                # print('metric_count : {}'.format(metric_counts))
                for metric_name, metric_config in metric_configs.items():
                    if metric_name not in metric_nb.keys():
                        metric_nb[metric_name] = metric_files.copy()
                    metric_nb[metric_name][date] = \
                        metric_nb[metric_name][date] + \
                        metric_counts[metric_config.label]

    # print('metric_nb : {}'.format(metric_nb))

    # Create the metric dataframe for the EO dataset
    eo_metrics_df = pd.DataFrame({'days': time_range,
                                  'eo_dataset_name': eo_dataset_name})
    for metric_name, metric_count in metric_nb.items():
        eo_metrics_df[metric_name] = metric_count.values()

    # print(eo_metrics_df)
    return eo_metrics_df


def compute_metric_section(metrics_df: DataFrame,
                           day: datetime,
                           eo_dataset_name: str,
                           ) -> MetricsSection:
    """
    Compute a report metrics section from metrics dataframe for a given day and
    an given EO dataset.
    Args:
        metrics_df : The dataframe containing the metrics counters.
        day : The day to compute.
        eo_dataset_name : The EO dataset name to compute.
    Returns:
        A report metrics section.
    Raises:
        None.
    """

    metric_section = None
    if metrics_df.empty is False:
        # Filter with the EO dataset
        metrics_subdf = metrics_df[metrics_df.eo_dataset_name.isin(
            [eo_dataset_name])].copy()
        # Drop the columns with Nan values
        metrics_subdf.dropna(axis=1, inplace=True)
        metrics_subdf = \
            metrics_subdf.set_index(['days', 'eo_dataset_name'])
        if metrics_subdf.empty is False:
            metric_section = MetricsSection(
                metrics_data=metrics_subdf.loc[day, eo_dataset_name].to_dict())

    return metric_section
