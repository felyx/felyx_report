import collections
import glob
import logging
import sys
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, OrderedDict, Tuple

import dateutil.parser as parser
import pandas as pd
from pandas import DataFrame, Timestamp

from felyx_processor.sites.insitu_factory import InSituDataSourceFactory
from felyx_processor.utils.configuration import FelyxProcessorConfig, FelyxSystemConfig
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_report.core.models import EO_source, MMDBReport, ProcessedFilesSection
from felyx_report.core.parameters import FelyxReportParams
from felyx_report.report.generate import mdb_extracter, metric_extracter

logger = logging.getLogger('felyx.report')

# Time patterns to replace for EO source file extraction
TIME_PATTERNS = ['%H', '%M', '%S']


def search_eo_files_infos(eo_file_path: str,
                          time_range: List[Timestamp]
                          ) -> OrderedDict[Timestamp, int]:
    """
    Compute the number of EO files per day of a time range
    Args:
        eo_file_path : The EO files path.
        time_range : The time range to compute.
    Returns:
        a dictionary containing the number of EO source files for
        each timestamp of the time range.
    """

    # List EO files in the EO source directory
    nb_eo_files = dict.fromkeys(time_range, 0)
    nb_eo_files = collections.OrderedDict(sorted(nb_eo_files.items()))

    # Replace time patterns by ?? character
    for pattern in TIME_PATTERNS:
        eo_file_path = eo_file_path.replace(pattern, '??')

    # print('eo path : {}'.format(eo_file_path))

    for date in nb_eo_files.keys():
        nb_files = 0
        # print('eo path  date : {}'.format(date.strftime(eo_file_path)))
        for file in glob.glob(date.strftime(eo_file_path)):
            if Path(file).exists():
                nb_files = nb_files + 1
        nb_eo_files[date] = nb_files

    # print('nb eo sources files : {}'.format(nb_eo_files))
    return nb_eo_files


def search_manifest_infos(manifest_root_dir: Path,
                          eo_dataset_name: str,
                          time_range: List[Timestamp],
                          site_collection: str,
                          ) -> Tuple[OrderedDict[Timestamp, int],
                                     OrderedDict[Timestamp, int]]:
    """
    Compute the number of manifest files and the number of matchups for each
    timestamp of the time range.
    Args:
        manifest_root_dir : The manifest root directory.
        eo_dataset_name : The eo dataset name
        time_range : The time range to compute.
        site_collection : The site collection identifier.
    Returns:
        a dictionary containing the number of manifest files and
        a dictionary containing the number matchups inside the manifests
        for each timestamp of the time range.
    Raises:
        FelyxProcessorError : When manifest file reading failure.
    """

    # List manifest file in the manifest directory
    # root_dir/eo_dataset_name/Y/J/*.manifest
    manifest_path = manifest_root_dir / eo_dataset_name \
                    / Path('%Y/%j/*.manifest')
    # print('manifest path : {}'.format(manifest_path))

    nb_manifest_files = dict.fromkeys(time_range, 0)
    nb_manifest_files = collections.OrderedDict(sorted(nb_manifest_files.items()))
    nb_manifest_matchups = dict.fromkeys(time_range, 0)
    nb_manifest_matchups = collections.OrderedDict(sorted(nb_manifest_matchups.items()))

    for date in nb_manifest_files.keys():
        nb_files = 0
        nb_manifests = 0
        for file in glob.glob(date.strftime(str(manifest_path.resolve()))):
            if Path(file).exists():
                try:
                    manifest_df = pd.read_json(file, lines=True)
                    # print('manifest file : {}'.format(manifest_df))
                    # Count the matchups belonging to the mmdb collection
                    matchups_nb = 0
                    if not manifest_df.empty:
                        matchups_nb = manifest_df[
                            manifest_df.site_collection == site_collection
                            ].site_collection.count()
                except IOError:
                    msg = 'Error while reading manifest file : {}'.format(file)
                    logger.error(msg)
                    raise FelyxProcessorError(msg)
                nb_manifests = nb_manifests + matchups_nb
                # Empty manifest files are also counted
                nb_files = nb_files + 1

        nb_manifest_files[date] = nb_files
        nb_manifest_matchups[date] = nb_manifests

    # print('nb manifest files : {} nb matchuos : {}'.format(nb_manifest_files, nb_manifest_matchups))

    return tuple((nb_manifest_files, nb_manifest_matchups))


def search_insitu_infos(site_collection: str,
                        felyx_sys_config: FelyxSystemConfig,
                        felyx_processor_config: FelyxProcessorConfig,
                        start: datetime,
                        end: datetime,
                        ) -> OrderedDict[Timestamp, int]:
    """
    Compute the number of measurements in Insitu collection for each
    timestamp of the time range.
    Args:
        felyx_sys_config : The Felyx system configuration.
        felyx_processor_config : The Felyx processor config.
        site_collection : The site collection identifier.
        start : The time range start to compute.
        end : The time range end to compute.
    Returns:
        a dictionary containing the number of insitu measurements
        for each timestamp of the time range.
    """

    # Get Insitu measurements infos
    insitu_ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=site_collection)

    # Retrieve measurements from Insitu datasource
    nb_measurements = insitu_ds.get_raws_count_by_day(
        collection_code=site_collection,
        start=start, stop=end)
    return nb_measurements


def generate_mmdb_reports(
        felyx_sys_config: FelyxSystemConfig,
        felyx_processor_config: FelyxProcessorConfig,
        felyx_report_params: FelyxReportParams,
) -> List[MMDBReport]:
    """
    Generate report data from MMDB configuration and manifest, metrics and
    mmdb files.
    Args:
        felyx_sys_config : The system configuration file.
        felyx_processor_config : The processor configuration file.
        felyx_report_params : The commands options.

    Returns:
        A list of report (per mmdb config)
    Raises:
        FelyxProcessorError
    """
    logger.info('_generate_mmdb_reports')

    # Init the time range
    try:
        start = parser.parse(str(felyx_report_params.start))
        end = parser.parse(str(felyx_report_params.stop))
        # Round start and end dates
        start = datetime(start.year, start.month, start.day)
        end = datetime(end.year, end.month, end.day)
        end = end + timedelta(seconds=24 * 3600 - 1)
    except ValueError:
        _, e, _ = sys.exc_info()
        msg = 'Error while parsing time range : {}'.format(str(e))
        logger.error(msg)
        raise FelyxProcessorError(msg)

    time_range = pd.date_range(start=start, end=end, freq='D')
    days = list(pd.to_datetime(time_range))
    # print('days : {} type : {}'.format(days, type(days)))

    # Get input directories
    manifest_root_dir = felyx_report_params.manifest_dir
    mdb_dir = felyx_report_params.mdb_dir
    metric_root_dir = felyx_report_params.metric_dir

    # For each MMDB processing
    mmdb_reports = list()
    for mmdataset_name, mmdataset_config in \
            felyx_processor_config.MMDatasets.items():

        # Compute processed input files
        # For each EO source
        input_files_df = DataFrame()
        metrics_df = DataFrame()
        for eodataset_name, eodataset_config in \
                mmdataset_config.eo_datasets.items():
            # Get the EO source files infos
            eo_filenaming = \
                felyx_processor_config.EODatasets[eodataset_name].filenaming

            eo_files_infos = search_eo_files_infos(eo_file_path=eo_filenaming,
                                                   time_range=days)
            # Get the manifest file infos
            manifest_file_nb, matchups_nb = search_manifest_infos(
                manifest_root_dir=manifest_root_dir,
                eo_dataset_name=eodataset_name, time_range=days,
                site_collection=mmdataset_config.site_collection_id)

            eo_files_df = pd.DataFrame({'days': days,
                                        'eo_dataset_name': eodataset_name})
            eo_files_df['eo_processed_files_nb'] = eo_files_infos.values()
            eo_files_df['manifest_files_nb'] = manifest_file_nb.values()
            eo_files_df['matchups_nb'] = matchups_nb.values()
            # print(eo_files_df)

            input_files_df = pd.concat([input_files_df, eo_files_df],
                                       ignore_index=True)

            # print('input_files_df : {}'.format(input_files_df))
            # Get the metric file infos
            if eodataset_config.metrics is not None \
                    and metric_root_dir is not None:
                eo_metrics_df = metric_extracter.search_metrics_infos(
                    eo_dataset_name=eodataset_name,
                    site_collection=mmdataset_config.site_collection_id,
                    metric_configs=eodataset_config.metrics,
                    metric_root_dir=metric_root_dir,
                    time_range=days)

                metrics_df = pd.concat([metrics_df, eo_metrics_df],
                                       ignore_index=True)
                # print('metrics_df : {}'.format(metrics_df))

        # Get Insitu measurements infos
        insitu_measurement_nb = search_insitu_infos(
            felyx_processor_config=felyx_processor_config,
            felyx_sys_config=felyx_sys_config,
            site_collection=mmdataset_config.site_collection_id,
            start=start, end=end)

        # Get matchups infos
        matchups_df = mdb_extracter.search_matchups_infos(
            mdb_output_config=mmdataset_config.output,
            mdb_root_dir=mdb_dir,
            start=start, end=end
        )

        # For each day of the time range
        for day in days:
            eo_sources = dict()
            # For each EO source
            for eo_dataset_name, subdf in input_files_df. \
                    groupby('eo_dataset_name'):
                # Construct processed files section
                subdf = subdf.set_index(['days'])
                processed_files_section = ProcessedFilesSection(
                    nb_processed_eo_files=subdf.loc[day,
                                                    'eo_processed_files_nb'],
                    nb_manifests=subdf.loc[day, 'manifest_files_nb'])

                # Construct matchups section
                matchups_section = mdb_extracter.compute_matchups_section(
                    matchups_df=matchups_df,
                    day=day,
                    eo_dataset_name=eo_dataset_name,
                    mdb_output_config=mmdataset_config.output
                )
                matchups_section.nb_matchups_manifest = \
                    subdf.loc[day, 'matchups_nb']

                # Construct the metrics section
                metric_section = metric_extracter.compute_metric_section(
                    metrics_df=metrics_df,
                    day=day,
                    eo_dataset_name=eo_dataset_name,
                )

                # Construct the EO sources
                eo_source = EO_source(name=eo_dataset_name,
                                      processed_files=processed_files_section,
                                      metrics=metric_section,
                                      matchups=matchups_section)
                if day not in eo_sources.keys():
                    eo_sources[day] = dict()
                eo_sources[day][eo_dataset_name] = eo_source

            # Construct MMDB report
            mmdb_report = MMDBReport(
                name=mmdataset_name,
                report_date=day.to_pydatetime(),
                eo_sources=list(eo_sources[day].values()),
                nb_in_situ_measurements=insitu_measurement_nb[day],
                collection_name=mmdataset_config.site_collection_id,
            )
            # export_mmdb_report = mmdb_report.export_model(index_prefix='prefix')
            # print("mmdb_report : {}".format(export_mmdb_report))
            mmdb_reports.append(mmdb_report)

    return mmdb_reports
