"""Handle Grafana notifications."""

from typing import Dict, List, NoReturn

from felyx_report.core.configuration import ContactPointConfig, RepeatIntervalConfig
from felyx_report.monitoring.grafana.api_client import GrafanaAPIClient


def create_contact_points(
    grafana_api_client: GrafanaAPIClient,
    contact_point_configs: List[ContactPointConfig],
    mmdb_name: str,
) -> Dict[str, RepeatIntervalConfig]:
    """Create a Grafana contact point from contact point configurations using Grafana Api client.

    Args:
        grafana_api_client: The Grafana API client
        contact_point_configs: The contact points configurations
        mmdb_name: The mmdb name

    Returns:
        The contact points dictionary
    """
    contact_points_dict = {}
    for contact_point_config in contact_point_configs:
        contact_point_name = '{0}_{1}_{2}'.format('felyx_report', contact_point_config.name, mmdb_name)
        contact_point = {
            'name': contact_point_name,
            'type': 'email',
            'settings': {
                'addresses': ';'.join(contact_point_config.addresses),
                'singleEmail': contact_point_config.single_email
            },
            'disableResolveMessage': contact_point_config.disable_resolve_message,
        }
        grafana_api_client.create_contact_point(contact_point=contact_point)
        contact_points_dict[contact_point_name] = contact_point_config.repeat_interval
    return contact_points_dict


def create_notification_policies(
    grafana_api_client: GrafanaAPIClient,
    mmdb_name: str,
    dashboard_uid: str,
    contact_points_dict: Dict[str, RepeatIntervalConfig],
) -> NoReturn:
    """Create Grafana notification policies using Grafana Api client.

    Args:
        grafana_api_client: The Grafana API client
        mmdb_name: The mmdb name
        dashboard_uid: The dashboard uid
        contact_points_dict: The contact points dictionary
    """
    for contact_point_name, repeat_interval_config in contact_points_dict.items():
        notification_policy = {
            'receiver': contact_point_name,
            'object_matchers': [
               [
                  'application_name',
                  '=',
                  'felyx_report',
               ],
               [
                  'mmdb_name',
                  '=',
                  mmdb_name,
               ],
               [
                   'dashboard_uid',
                   '=',
                   dashboard_uid,
               ],
            ],
            'repeat_interval': '{0}{1}'.format(repeat_interval_config.duration, repeat_interval_config.unit),
        }
        grafana_api_client.create_notification_policy(notification_policy=notification_policy)
