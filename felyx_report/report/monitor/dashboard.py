"""Create MMDB new dashboards."""

import json
from pathlib import Path
from typing import Any, Dict, List, NoReturn, Optional

from felyx_report.core.configuration import ContactPointConfig, FelyxReportProcessorConfig, GrafanaConfiguration
from felyx_report.core.exceptions import FelyxReportError
from felyx_report.core.models import MMDBReport
from felyx_report.core.templating import render_json_template
from felyx_report.monitoring.grafana.api_client import GrafanaAPIClient
from felyx_report.report.monitor import alerting, notifying

NB_MMDB_FILES = 'nb_mmdb_files'
NB_ASSEMBLED_MATCHUPS = 'nb_assembled_matchups'
NB_METRIC = 'nb_metric'
COMMA = ','
EO_SOURCE_NAME = 'eo_source_name'


def build_mmdb_eo_sources_dict(  # noqa: WPS231
    mmdb_reports: List[MMDBReport],
) -> Dict[str, Dict[str, Dict[str, List[str]]]]:  # noqa: WPS221
    """Build the MMDB eo_sources dynamic fields dictionary.

    Args:
        mmdb_reports: The MMDBReports list

    Returns:
        A dictionary with MMDB names as keys and a dictionary of the dynamic fields as values per eo_source.
    """
    mmdb_eo_sources_dict = {}
    for mmdb_report in mmdb_reports:
        if mmdb_report.name in mmdb_eo_sources_dict.keys():
            for eo_source in mmdb_report.eo_sources:
                matchups = eo_source.matchups
                metrics = eo_source.metrics
                if eo_source.name not in mmdb_eo_sources_dict[mmdb_report.name].keys():
                    mmdb_eo_sources_dict[mmdb_report.name][eo_source.name] = {
                        NB_MMDB_FILES: list(matchups.nb_mmdb_files.keys()),  # products list
                        NB_ASSEMBLED_MATCHUPS: list(matchups.nb_assembled_matchups.keys()),  # products list
                        NB_METRIC: list(metrics.metrics_data.keys()) if metrics else [],  # metrics list
                    }
                else:
                    mmdb_eo_sources_dict[mmdb_report.name][eo_source.name][NB_MMDB_FILES] = list(
                        set(mmdb_eo_sources_dict[mmdb_report.name][eo_source.name][NB_MMDB_FILES]).union(
                            set(matchups.nb_mmdb_files.keys()),
                        ),
                    )
                    mmdb_eo_sources_dict[mmdb_report.name][eo_source.name][NB_ASSEMBLED_MATCHUPS] = list(
                        set(mmdb_eo_sources_dict[mmdb_report.name][eo_source.name][NB_ASSEMBLED_MATCHUPS]).union(
                            set(matchups.nb_assembled_matchups.keys()),
                        ),
                    )
                    if metrics:
                        mmdb_eo_sources_dict[mmdb_report.name][eo_source.name][NB_METRIC] = list(  # noqa: WPS220
                            set(mmdb_eo_sources_dict[mmdb_report.name][eo_source.name][NB_METRIC]).union(
                                set(metrics.metrics_data.keys()),
                            ),
                        )
        else:
            for mmdb_eo_source in mmdb_report.eo_sources:
                matchups = mmdb_eo_source.matchups
                metrics = mmdb_eo_source.metrics
                mmdb_eo_sources_dict[mmdb_report.name] = {
                    mmdb_eo_source.name: {
                        NB_MMDB_FILES: list(matchups.nb_mmdb_files.keys()),  # products list
                        NB_ASSEMBLED_MATCHUPS: list(matchups.nb_assembled_matchups.keys()),  # products list
                        NB_METRIC: list(metrics.metrics_data.keys()) if metrics else [],  # metrics list
                    },
                }

    return mmdb_eo_sources_dict


def _render_input_dataset_processing(
    template_dir: Path,
    common_template_params: Dict[str, Any],
    eo_sources_dict: Dict[str, Dict[str, List[str]]],
) -> List[str]:
    # a dashboard panel per eo_source under "Input data processing"
    input_dataset_processing_rendered = []
    for eo_source_name in eo_sources_dict:
        input_dataset_processing_rendered.append(
            render_json_template(
                template_dir=template_dir,
                template_name='input_dataset_processing.tpl',
                template_params={
                    **common_template_params,
                    **{EO_SOURCE_NAME: eo_source_name},
                },
            ),
        )
    return input_dataset_processing_rendered


def _render_metrics_processing(
    template_dir: Path,
    common_template_params: Dict[str, Any],
    eo_sources_dict: Dict[str, Dict[str, List[str]]],
) -> List[str]:
    # a dashboard panel per eo_source and per metric under "Metrics processing"
    metrics_processing_rendered = []
    for eo_source_name, dynamic_fields in eo_sources_dict.items():
        if NB_METRIC in dynamic_fields.keys():
            for nb_metric in dynamic_fields[NB_METRIC]:
                metrics_processing_rendered.append(
                    render_json_template(
                        template_dir=template_dir,
                        template_name='metrics_processing.tpl',
                        template_params={
                            **common_template_params,
                            **{
                                EO_SOURCE_NAME: eo_source_name,
                                'metric': nb_metric,
                            },
                        },
                    ),
                )
    return metrics_processing_rendered


def _render_multi_matchup_dataset_files(
    template_dir: Path,
    common_template_params: Dict[str, Any],
    eo_sources_dict: Dict[str, Dict[str, List[str]]],
) -> List[str]:
    # a dashboard panel per eo_source and per product under "Multi-Matchup Dataset files"
    multi_matchup_dataset_files_rendered = []
    for eo_source_name, dynamic_fields in eo_sources_dict.items():
        for nb_mmdb_files_product in dynamic_fields[NB_MMDB_FILES]:
            multi_matchup_dataset_files_rendered.append(
                render_json_template(
                    template_dir=template_dir,
                    template_name='multi_matchup_dataset_files.tpl',
                    template_params={
                        **common_template_params,
                        **{
                            EO_SOURCE_NAME: eo_source_name,
                            'product': nb_mmdb_files_product,
                        },
                    },
                ),
            )
    return multi_matchup_dataset_files_rendered


def _render_number_of_matchups(
    template_dir: Path,
    common_template_params: Dict[str, Any],
    eo_sources_dict: Dict[str, Dict[str, List[str]]],
    mmdb_name: str,
    felyx_report_processor_config: FelyxReportProcessorConfig,
) -> List[str]:
    # a dashboard panel per eo_source and per product under "Number of matchups"
    number_of_matchups_rendered = []
    for eo_source_name, dynamic_fields in eo_sources_dict.items():
        for nb_assembled_matchups_product in dynamic_fields[NB_ASSEMBLED_MATCHUPS]:
            if felyx_report_processor_config.MMDatasets[mmdb_name].output.\
                    products[nb_assembled_matchups_product].\
                    content[eo_source_name].monitoring.\
                    dashboard_create_nb_of_created_matchups:
                number_of_matchups_rendered.append(
                    render_json_template(
                        template_dir=template_dir,
                        template_name='number_of_matchups.tpl',
                        template_params={
                            **common_template_params,
                            **{
                                EO_SOURCE_NAME: eo_source_name,
                                'product': nb_assembled_matchups_product,
                            },
                        },
                    ),
                )
    return number_of_matchups_rendered


def create_mmdb_dashboard(  # noqa: WPS211
    grafana_api_client: GrafanaAPIClient,
    title: str,
    datasource_uid: str,
    eo_sources_dict: Dict[str, Dict[str, List[str]]],
    mmdb_name: str,
    felyx_report_processor_config: FelyxReportProcessorConfig,
    overwrite: bool,
    dashboard_uid: Optional[str] = None,
) -> str:
    """Create the Grafana MMDB dashboard.

    Args:
        grafana_api_client: The Grafana API client
        title: The dashboard title
        datasource_uid: The Grafana datasource uid
        eo_sources_dict: The MMDB eo_sources dynamic fields dictionary
        mmdb_name: The mmdb name
        felyx_report_processor_config : The Felyx report processor configuration
        overwrite: Boolean indicating whether the dashboard should be overwritten
        dashboard_uid: The existing dashboard uid in case of update

    Returns:
        The dashboard uid
    """
    grafana_config = grafana_api_client.config
    template_dir = Path(Path(__file__).parent.parent.parent, 'share', 'grafana', 'dashboard')

    time_range_from_days = grafana_api_client.config.dashboards.time_range_from_days
    time_range_to_days = grafana_api_client.config.dashboards.time_range_to_days

    common_template_params = {
        'title': title,
        'folderId': grafana_api_client.get_root_folder_id(),
        'pluginVersion': grafana_config.dashboards.plugin_version,
        'timeField': grafana_config.datasources.time_field_name,
        'datasourceType': grafana_config.datasources.type,
        'datasourceUid': datasource_uid,
        'overwrite': overwrite,
        'dashboardTimeRangeFrom': time_range_from_days,
        'dashboardTimeRangeTo': time_range_to_days,
    }

    input_dataset_processing_rendered = _render_input_dataset_processing(
        template_dir=template_dir,
        common_template_params=common_template_params,
        eo_sources_dict=eo_sources_dict,
    )

    metrics_processing_rendered = _render_metrics_processing(
        template_dir=template_dir,
        common_template_params=common_template_params,
        eo_sources_dict=eo_sources_dict,
    )

    matchups_with_cmems_drifters_rendered = render_json_template(
        template_dir=template_dir,
        template_name='matchups_with_cmems_drifters.tpl',
        template_params=common_template_params,
    )

    multi_matchup_dataset_files_rendered = _render_multi_matchup_dataset_files(
        template_dir=template_dir,
        common_template_params=common_template_params,
        eo_sources_dict=eo_sources_dict,
    )

    number_of_matchups_rendered = _render_number_of_matchups(
        template_dir=template_dir,
        common_template_params=common_template_params,
        eo_sources_dict=eo_sources_dict,
        felyx_report_processor_config=felyx_report_processor_config,
        mmdb_name=mmdb_name,
    )

    template_params = {
        **common_template_params,
        **{
            'input_dataset_processing': COMMA.join(input_dataset_processing_rendered),
            'metrics_processing': COMMA.join(metrics_processing_rendered)
            if metrics_processing_rendered else None,
            'matchups_with_cmems_drifters': matchups_with_cmems_drifters_rendered,
            'multi_matchup_dataset_files': COMMA.join(multi_matchup_dataset_files_rendered),
            'number_of_matchups': COMMA.join(number_of_matchups_rendered)
            if number_of_matchups_rendered or len(number_of_matchups_rendered) == 0 else None,
        },
    }

    if dashboard_uid:
        mmdb_alert_list_rendered = render_json_template(
            template_dir=template_dir,
            template_name='mmdb_alert_list.tpl',
            template_params={
                **common_template_params,
                'dashboard_uid': dashboard_uid,
            },
        )

        template_params = {
            **template_params,
            **{
                'dashboard_uid': dashboard_uid,
                'mmdb_alert_list': mmdb_alert_list_rendered,
            },
        }

    return grafana_api_client.create_dashboard(
        dashboard_json=json.loads(
            render_json_template(
                template_dir=template_dir,
                template_name='mmdb_dashboard_template.tpl',
                template_params=template_params,
            ),
        ),
    )


def process(
    mmdb_reports: List[MMDBReport],
    grafana_config: GrafanaConfiguration,
    es_prefix: str,
    contact_point_configs: List[ContactPointConfig],
    felyx_report_processor_config: FelyxReportProcessorConfig,
) -> NoReturn:
    """Create the Grafana dashboards if not already existing.

    Args:
        mmdb_reports: The MMDBReports list
        grafana_config: The Grafana configuration
        es_prefix: The Elasticsearch index prefix
        contact_point_configs: The contact points configurations
        felyx_report_processor_config: The Felyx report processor configuration

    Raises:
        FelyxReportError: In case of error while retrieving datasource uid
    """
    grafana_api_client = GrafanaAPIClient(grafana_config=grafana_config)
    mmdb_eo_sources_dict = build_mmdb_eo_sources_dict(mmdb_reports)
    try:
        datasource_uid = grafana_api_client.client.datasource.get_datasource_by_name(es_prefix)['uid']
    except Exception as exception:
        raise FelyxReportError('Unable to retrieve uid with datasource name {0} : {1}'.format(es_prefix, exception))

    for mmdb_name, eo_sources_dict in mmdb_eo_sources_dict.items():
        contact_points_dict = notifying.create_contact_points(
            grafana_api_client=grafana_api_client,
            contact_point_configs=contact_point_configs,
            mmdb_name=mmdb_name,
        )
        dashboard_uid = None
        if grafana_api_client.dashboard_exists(dashboard_name=mmdb_name):
            dashboard_uid = grafana_api_client.get_dashboard_uid_by_name(dashboard_name=mmdb_name)
            grafana_api_client.delete_notification_policies(contact_point_names=list(contact_points_dict.keys()))
            alerting.delete_alert_rules(grafana_api_client=grafana_api_client, dashboard_uid=dashboard_uid)
        if not dashboard_uid:
            dashboard_uid = create_mmdb_dashboard(
                grafana_api_client=grafana_api_client,
                title=mmdb_name,
                datasource_uid=datasource_uid,
                eo_sources_dict=eo_sources_dict,
                mmdb_name=mmdb_name,
                felyx_report_processor_config=felyx_report_processor_config,
                overwrite=False,
            )
        # dashboard update / alert list addition after creation
        create_mmdb_dashboard(
            grafana_api_client=grafana_api_client,
            title=mmdb_name,
            datasource_uid=datasource_uid,
            eo_sources_dict=eo_sources_dict,
            mmdb_name=mmdb_name,
            felyx_report_processor_config=felyx_report_processor_config,
            overwrite=True,
            dashboard_uid=dashboard_uid,
        )
        alerting.create_alert_rules(
            grafana_api_client=grafana_api_client,
            datasource_uid=datasource_uid,
            mmdb_name=mmdb_name,
            eo_sources_dict=eo_sources_dict,
            dashboard_uid=dashboard_uid,
            felyx_report_processor_config=felyx_report_processor_config,
        )
        notifying.create_notification_policies(
            grafana_api_client=grafana_api_client,
            mmdb_name=mmdb_name,
            dashboard_uid=dashboard_uid,
            contact_points_dict=contact_points_dict,
        )
