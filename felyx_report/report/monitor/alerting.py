"""Handle Grafana alert rules."""

from pathlib import Path
from typing import Any, Dict, List, NoReturn

from felyx_report.core.configuration import FelyxReportProcessorConfig
from felyx_report.core.templating import render_json_template
from felyx_report.monitoring.grafana.api_client import GrafanaAPIClient


def _create_nb_of_created_matchups_vs_assembled_matchups_rule(
    grafana_api_client: GrafanaAPIClient,
    template_dir: Path,
    template_params: Dict[str, Any],
):
    alert_rule_rendered = render_json_template(
        template_dir=template_dir,
        template_name='nb_of_created_matchups_vs_assembled_matchups.tpl',
        template_params=template_params,
    )
    grafana_api_client.create_alert_rule(alert_rule=alert_rule_rendered)


def _create_nb_of_created_metrics_rule(
    grafana_api_client: GrafanaAPIClient,
    template_dir: Path,
    template_params: Dict[str, Any],
):
    alert_rule_rendered = render_json_template(
        template_dir=template_dir,
        template_name='nb_of_created_metrics_vs_created_matchups.tpl',
        template_params=template_params,
    )
    grafana_api_client.create_alert_rule(alert_rule=alert_rule_rendered)


def _create_nb_of_in_situ_measurements_rule(
    grafana_api_client: GrafanaAPIClient,
    template_dir: Path,
    template_params: Dict[str, Any],
):
    alert_rule_rendered = render_json_template(
        template_dir=template_dir,
        template_name='nb_of_in_situ_measurements.tpl',
        template_params=template_params,
    )
    grafana_api_client.create_alert_rule(alert_rule=alert_rule_rendered)


def _create_nb_of_processed_eo_files_vs_result_manifests_rule(
    grafana_api_client: GrafanaAPIClient,
    template_dir: Path,
    template_params: Dict[str, Any],
):
    alert_rule_rendered = render_json_template(
        template_dir=template_dir,
        template_name='nb_of_processed_eo_files_vs_result_manifests.tpl',
        template_params=template_params,
    )
    grafana_api_client.create_alert_rule(alert_rule=alert_rule_rendered)


def _create_nb_of_created_mmdb_files_rule(
    grafana_api_client: GrafanaAPIClient,
    template_dir: Path,
    template_params: Dict[str, Any],
):
    alert_rule_rendered = render_json_template(
        template_dir=template_dir,
        template_name='nb_of_created_mmdb_files.tpl',
        template_params=template_params,
    )
    grafana_api_client.create_alert_rule(alert_rule=alert_rule_rendered)


def delete_alert_rules(grafana_api_client: GrafanaAPIClient, dashboard_uid: str) -> NoReturn:
    """Delete alert rules by dashboard uid (label).

    Args:
        grafana_api_client: The Grafana API client
        dashboard_uid: The dashboard uid
    """
    grafana_api_client.delete_alert_rules(dashboard_uid=dashboard_uid)


def create_alert_rules(
    grafana_api_client: GrafanaAPIClient,
    datasource_uid: str,
    mmdb_name: str,
    eo_sources_dict: Dict[str, Dict[str, List[str]]],
    dashboard_uid: str,
    felyx_report_processor_config: FelyxReportProcessorConfig,
) -> NoReturn:
    """Create Grafana alert rules.

    Args:
        grafana_api_client: The Grafana API client
        datasource_uid: The Grafana datasource uid
        mmdb_name: The mmdb name
        eo_sources_dict: The MMDB eo_sources dynamic fields dictionary
        dashboard_uid: The dashboard uid
        felyx_report_processor_config : The Felyx report processor configuration
    """
    template_dir = Path(Path(__file__).parent.parent.parent, 'share', 'grafana', 'alert_rules')

    for eo_source_name in eo_sources_dict:
        common_alert_params = {
            'timeField': grafana_api_client.config.datasources.time_field_name,
            'datasourceUid': datasource_uid,
            'mmdb_name': mmdb_name,
            'eo_source_name': eo_source_name,
            'alertIntervall':
                grafana_api_client.config.alert_rules.alert_evaluation_period,
            'alertFor':
                grafana_api_client.config.alert_rules.alert_firing_delay,
            'alertTimeRangeFrom':
                grafana_api_client.config.alert_rules.alert_relative_timerange_start,
            'alertTimeRangeTo':
                grafana_api_client.config.alert_rules.alert_relative_timerange_end,
            'dashboard_uid': dashboard_uid,
        }

        collection_name = felyx_report_processor_config.MMDatasets[mmdb_name].site_collection_id
        alert_threshold_nb_of_in_situ_measurements = felyx_report_processor_config.SiteCollections[collection_name].monitoring.alert_threshold_nb_of_in_situ_measurements
        if alert_threshold_nb_of_in_situ_measurements != -1:
            _create_nb_of_in_situ_measurements_rule(
                grafana_api_client=grafana_api_client,
                template_dir=template_dir,
                template_params={
                    **common_alert_params,
                    **{'alert_threshold': alert_threshold_nb_of_in_situ_measurements}
                },
            )

        if felyx_report_processor_config.EODatasets[eo_source_name].monitoring.alert_check_nb_of_processed_eo_files:
            _create_nb_of_processed_eo_files_vs_result_manifests_rule(
                grafana_api_client=grafana_api_client,
                template_dir=template_dir,
                template_params=common_alert_params,
            )

        for product in eo_sources_dict[eo_source_name]['nb_assembled_matchups']:
            if felyx_report_processor_config.MMDatasets[mmdb_name].output.products[product].content[eo_source_name].monitoring.alert_check_nb_of_created_matchups:
                _create_nb_of_created_matchups_vs_assembled_matchups_rule(
                    grafana_api_client=grafana_api_client,
                    template_dir=template_dir,
                    template_params={
                        **common_alert_params,
                        **{'product': product},
                    },
                )

        if 'nb_metric' in eo_sources_dict[eo_source_name].keys():
            for metric in eo_sources_dict[eo_source_name]['nb_metric']:
                if felyx_report_processor_config.MMDatasets[mmdb_name].eo_datasets[eo_source_name].metrics[metric].monitoring.alert_check_nb_metrics:
                    _create_nb_of_created_metrics_rule(
                        grafana_api_client=grafana_api_client,
                        template_dir=template_dir,
                        template_params={
                            **common_alert_params,
                            **{
                                'metric': metric
                            },
                        },
                    )
        for product in eo_sources_dict[eo_source_name]['nb_mmdb_files']:
            alert_threshold_nb_of_created_mmdb_files = felyx_report_processor_config.MMDatasets[mmdb_name].output.products[product].content[eo_source_name].monitoring.alert_threshold_nb_of_created_mmdb_files
            if alert_threshold_nb_of_created_mmdb_files != -1:
                _create_nb_of_created_mmdb_files_rule(
                    grafana_api_client=grafana_api_client,
                    template_dir=template_dir,
                    template_params={
                        **common_alert_params,
                        **{
                            'product': product,
                            'alert_threshold': alert_threshold_nb_of_created_mmdb_files,
                        },
                    },
                )
