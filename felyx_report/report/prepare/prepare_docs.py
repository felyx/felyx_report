"""Prepare docs for indexation."""

from pathlib import Path
from typing import Dict, List, Tuple

from felyx_processor.index.es.es_client import ESClient
from felyx_processor.utils.configuration import ElasticSearchConfig
from felyx_report.core.configuration import GrafanaConfiguration
from felyx_report.core.models import MMDBReport
from felyx_report.monitoring.grafana.api_client import GrafanaAPIClient


def _unpack_mmdb_reports(  # noqa: WPS234
    mmdb_reports: List[MMDBReport],
    index_prefix: str,
) -> Dict[str, List[Tuple[str, Dict]]]:
    """Create a dictionary per index containing tuples representing the reports to be indexed.

    Args:
        mmdb_reports: The list of MMDBReports to unpack
        index_prefix: The index prefix

    Returns:
        A dictionary representing the reports to be indexed
    """
    index_dict = {}
    for mmdb_report in mmdb_reports:
        export_list = mmdb_report.export_model(index_prefix=index_prefix)
        for export in export_list:
            if export['_index'] in index_dict.keys():
                index_dict[export['_index']].append((export['_id'], export))
            else:
                index_dict[export['_index']] = [(export['_id'], export)]
    return index_dict


def prepare_indexes_and_datasources(  # noqa: WPS234
    mmdb_reports: List[MMDBReport],
    es_config: ElasticSearchConfig,
    grafana_config: GrafanaConfiguration,
) -> Dict[str, List[Tuple[str, Dict]]]:
    """Create the Elasticsearch indexes end Grafana datasources if not already existing.

    Args:
        mmdb_reports: The MMDBReports list
        es_config: The Elasticsearch configuration
        grafana_config: The Grafana configuration

    Returns:
        A dictionary representing the documents to be indexed
    """
    es_client = ESClient(es_config=es_config)
    grafana_api_client = GrafanaAPIClient(grafana_config=grafana_config)
    es_mapping_path = Path(Path(__file__).parent.parent.parent, 'share', 'es', 'mmdb_report_mapping.json')

    unique_mmdb_names = {mmdb_report.name for mmdb_report in mmdb_reports}
    for mmdb_name in unique_mmdb_names:
        index_name = '{0}{1}'.format(es_config.prefix, mmdb_name.lower())
        if not es_client.has_index(index_path=index_name):
            es_client.create_index(index_path=index_name, mappings_path=es_mapping_path)
        if not grafana_api_client.folder_exists(folder_name=grafana_config.root_folder):
            grafana_api_client.create_root_folder()
        if not grafana_api_client.datasource_exists(name=es_config.prefix):
            grafana_api_client.create_grafana_es_datasource(
                name=es_config.prefix,
                es_index_name='{0}*'.format(es_config.prefix),
                time_field=grafana_config.datasources.time_field_name,
            )
    return _unpack_mmdb_reports(mmdb_reports=mmdb_reports, index_prefix=es_config.prefix)
