    {# ------------------ Graph - Number of metrics produced for EO dataset <eo_source_name> for metric: <metric> ------------------ #}
    {
      "datasource": {
        "type":{{datasourceType|jsonify}},
        "uid":{{datasourceUid|jsonify}}
      },
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisLabel": "",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 0,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineWidth": 1,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          }
        }
      },
      "gridPos": {
        "h": 8,
        "w": 24,
        "x": 0,
        "y": 26
      },
      "options": {
        "legend": {
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single",
          "sort": "none"
        }
      },
      "targets": [
        {
          "alias": "",
          "bucketAggs": [
            {
              "field":{{timeField|jsonify}},
              "id": "2",
              "settings": {
                "interval": "1d"
              },
              "type": "date_histogram"
            }
          ],
          "datasource": {
            "type":{{datasourceType|jsonify}},
            "uid":{{datasourceUid|jsonify}}
          },
          "metrics": [
            {
              "field": "nb_metric_{{metric}}",
              "id": "1",
              "type": "sum"
            }
          ],
          "query": "mmdb_name:\"{{title}}\" AND eo_source_name:\"{{eo_source_name}}\"",
          "refId": "A",
          "timeField": "report_date"
        }
      ],
      "title": "Number of metrics produced for input EO dataset {{eo_source_name}} for metric : {{metric}}",
      "type": "timeseries"
    }
