{
  "dashboard":
    {
      {# ------------------ panels ------------------ #}
      "panels":[
        {# ------------------ title Felyx Multi-Matchup Dataset Monitoring Dashboard ------------------ #}
        {
          "gridPos":{
             "h":2,
             "w":24,
             "x":0,
             "y":0
          },
          "options":{
             "content":"# Felyx Multi-Matchup Dataset Monitoring Dashboard",
             "mode":"markdown"
          },
          {# config.dashboards.plugin_version #}
          "pluginVersion":{{pluginVersion|jsonify}},
          "transparent":true,
          "type":"text"
        },
        {# ------------------ subtitle Multi-Matchup Dataset ------------------ #}
        {
          "gridPos":{
             "h":2,
             "w":24,
             "x":0,
             "y":2
          },
          "options":{
             "content":"## Multi-Matchup Dataset: **{{title}}**\n",
             "mode":"markdown"
          },
          "pluginVersion": {{pluginVersion|jsonify}},
          "type":"text"
        },
        {# ------------------ subtitle Input data processing ------------------ #}
        {
          "gridPos":{
             "h":3,
             "w":24,
             "x":0,
             "y":4
          },
          "options":{
             "content":"### Input data processing\n\nMonitor the completeness of the input files processing by verifying the number of processed input EO files vs the created output manifests per day and per EO dataset",
             "mode":"markdown"
          },
          "pluginVersion":{{pluginVersion|jsonify}},
          "type":"text"
        },
        {# ------------------ see input_dataset_processing.tpl ------------------ #}
        {# ------------------ a dashboard panel per eo_source under "Input data processing" ------------------ #}
        {{input_dataset_processing}}
        ,
        {%- if metrics_processing %}
        {# ------------------ subtitle Metrics processing ------------------ #}
        {
          "gridPos":{
             "h":3,
             "w":24,
             "x":0,
             "y":23
          },
          "options":{
             "content":"## Metrics processing\n\nMonitor the completeness of metrics production by verifying the number of created metrics per day, for each type of metrics, for a given input dataset.",
             "mode":"markdown"
          },
          "pluginVersion":{{pluginVersion|jsonify}},
          "type":"text"
        },
        {# ------------------ see metrics_processing.tpl ------------------ #}
        {# ------------------ a dashboard panel per eo_source and per metric under "Metrics processing" ------------------ #}
        {{metrics_processing}}
        ,
        {%- endif %}
        {# ------------------ see matchups_with_cmems_drifters.tpl ------------------ #}
        {{matchups_with_cmems_drifters}}
        ,
        {# ------------------ subtitle Multi-Matchup Dataset files ------------------ #}
        {
          "gridPos":{
             "h":2,
             "w":24,
             "x":0,
             "y":54
          },
          "options":{
             "content":"#### Multi-Matchup Dataset files\n\nMonitor the number of out MMDB files created per day, per EO dataset and per subproduct",
             "mode":"markdown"
          },
          "pluginVersion":{{pluginVersion|jsonify}},
          "type":"text"
        },
        {# ------------------ see multi_matchup_dataset_files.tpl ------------------ #}
        {# ------------------ a dashboard panel per eo_source and per product under "Multi-Matchup Dataset files" ------------------ #}
        {{multi_matchup_dataset_files}}
        ,
        {# ------------------ subtitle Number of matchups ------------------ #}
        {
           "gridPos":{
              "h":2,
              "w":24,
              "x":0,
              "y":64
           },
           "options":{
              "content":"#### Number of matchups\n\nMonitor the number of match-ups extracted per day, vs the number of match-ups assembled for the same day. They should be identical.",
              "mode":"markdown"
           },
           "pluginVersion":{{pluginVersion|jsonify}},
           "type":"text"
        },
        {# ------------------ see number_of_matchups.tpl ------------------ #}
        {# ------------------ a dashboard panel per eo_source and per product under "Number of matchups" ------------------ #}
        {{number_of_matchups}}
        {%- if dashboard_uid %}
        ,
        {# ------------------ subtitle MMDB Alert list ------------------ #}
        {
          "gridPos":{
             "h":3,
             "w":24,
             "x":0,
             "y":74
          },
          "options":{
             "content":"## MMDB Alert list\n\nMonitor the alerts for the MMDB",
             "mode":"markdown"
          },
          "pluginVersion":{{pluginVersion|jsonify}},
          "type":"text"
        }
        ,
        {# ------------------ see mmdb_alert_list.tpl ------------------ #}
        {# ------------------ an alert list panel listing alerts for the mmdb ------------------ #}
        {{mmdb_alert_list}}
        {%- endif %}
      ],
      "style": "dark",
      "time": {
        "from": "now-{{dashboardTimeRangeFrom|jsonify}}d",
        "to": "now-{{dashboardTimeRangeTo|jsonify}}d"
      },
      {# mmdb name #}
      "title":{{title|jsonify}}
      {# ------------------ in case of update ------------------ #}
      {%- if dashboard_uid %}
      ,
      "uid":{{dashboard_uid|jsonify}}
      {%- endif %}
    },
  {# config.root_folder #}
  "folderId": {{folderId|jsonify}},
  "overwrite": {{overwrite|jsonify}}
}
