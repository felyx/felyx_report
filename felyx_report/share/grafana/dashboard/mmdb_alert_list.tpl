    {# ------------------ List - Alert list for mmdb: <title> ------------------ #}
    {
      "datasource": {
        "type":{{datasourceType|jsonify}},
        "uid":{{datasourceUid|jsonify}}
      },
      "gridPos": {
        "h": 8,
        "w": 24,
        "x": 0,
        "y": 80
      },
      "options": {
        "groupMode": "default",
        "groupBy": [],
        "maxItems": 20,
        "sortOrder": 1,
        "dashboardAlerts": false,
        "alertName": "",
        "alertInstanceLabelFilter": "mmdb_name={{title}},dashboard_uid={{dashboard_uid}}",
        "stateFilter": {
          "firing": true,
          "pending": true,
          "inactive": false,
          "noData": false,
          "normal": false,
          "error": true
        }
      },
      "title": "Alert list for mmdb : {{title}}",
      "type": "alertlist"
    }
