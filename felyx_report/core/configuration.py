"""Felyx report configuration."""

import os
from enum import Enum
from pathlib import Path
from typing import Dict, List, Union

from pydantic import AnyHttpUrl

from felyx_processor.utils.configuration import (
    FELYX_CFG_SYS_ENV,
    ContentSelectionConfig,
    DatasetConfig,
    EODatasetExtractionConfig,
    FelyxModel,
    FelyxProcessorConfig,
    FelyxSystemConfig,
    MDatasetConfig,
    MDBOutputConfig,
    MetricConfig,
    ProductConfig,
    SiteCollectionConfig,
)
from felyx_report.core.exceptions import FelyxReportError


class GrafanaDatasourcesJsonDataConfiguration(FelyxModel):
    """Represents the Grafana datasources json data configuration model."""

    es_version: str = '7.10.0'
    include_frozen: bool = False
    max_concurrent_shard_requests: int = 5
    time_interval: str = '1d'


class GrafanaDatasourcesConfiguration(FelyxModel):
    """Represents the Grafana datasources configuration model."""

    type: str = 'elasticsearch'
    access: str = 'proxy'
    url: AnyHttpUrl = 'http://127.0.0.1:9200'
    password: str = ''
    user: str = ''
    basic_auth: bool = False
    time_field_name: str = 'report_date'
    json_data: GrafanaDatasourcesJsonDataConfiguration = GrafanaDatasourcesJsonDataConfiguration()


class GrafanaDashboardsConfiguration(FelyxModel):
    """Represents the Grafana dashboards configuration model."""

    plugin_version: str = '9.3.2'
    time_range_from_days: int = 90
    time_range_to_days: int = 0


class GrafanaSearchConfiguration(FelyxModel):
    """Represents the Grafana search configuration model."""

    dashboard_type: str = 'dash-db'


class GrafanaAlertRulesConfiguration(FelyxModel):
    """Represents the Grafana alert rules configuration model."""

    # How often the alert will be evaluated to see if it fires
    alert_evaluation_period: str = '1m'
    # Once condition is breached, alert will go into pending state.
    # If it is pending for longer than the "for" value, it will become a
    # firing alert.
    alert_firing_delay: str = '5m'
    # The relative timerange start in seconds from now of alert analysis
    alert_relative_timerange_start: int = 864000
    # The relative timerange end in seconds from now of alert analysis
    alert_relative_timerange_end: int = 0


class GrafanaConfiguration(FelyxModel):
    """Represents the Grafana configuration model."""

    host: str = 'grafana.domain.fr'
    api_key: str = 'api_key'
    ssl_verify: bool = False
    search: GrafanaSearchConfiguration = GrafanaSearchConfiguration()
    alert_rules: GrafanaAlertRulesConfiguration = GrafanaAlertRulesConfiguration()
    datasources: GrafanaDatasourcesConfiguration = GrafanaDatasourcesConfiguration()
    dashboards: GrafanaDashboardsConfiguration = GrafanaDashboardsConfiguration()
    root_folder: str = 'felyx_report'


class RepeatIntervalUnitEnum(str, Enum):
    """Felyx report mail notification repeat interval unit enumeration."""

    seconds = 's'
    minutes = 'm'
    hours = 'h'
    days = 'd'
    weeks = 'w'


class RepeatIntervalConfig(FelyxModel):
    """Felyx report mail notification repeat interval configuration."""

    duration: int = 4
    unit: RepeatIntervalUnitEnum = RepeatIntervalUnitEnum.hours


class ContactPointConfig(FelyxModel):
    """Felyx report contact point configuration."""

    name: str
    addresses: List[str]
    disable_resolve_message: bool
    single_email: bool
    repeat_interval: RepeatIntervalConfig = RepeatIntervalConfig()


class FelyxReportConfig(FelyxSystemConfig):
    """Felyx report configuration."""

    contact_points: List[ContactPointConfig] = []
    grafana: GrafanaConfiguration = GrafanaConfiguration()


def load_felyx_report_config(
    configuration_file: Path = None,
    override_data: Dict = None,
) -> FelyxReportConfig:
    """Load Felyx report configuration from a YAML File.

    Args:
        configuration_file: YAML configuration file
        override_data: override data

    Returns:
        FelyxReportConfig: an instance of FelyxReportConfig

    Raises:
        FelyxReportError: In case the provided configuration file does not exist.
    """
    if configuration_file is None and FELYX_CFG_SYS_ENV in os.environ:
        configuration_file = Path(os.environ[FELYX_CFG_SYS_ENV])
        if not configuration_file.exists():
            raise FelyxReportError(  # noqa: WPS462
                """
                The file {0} (provided by the environmental variable {1}
                is not available. As this file provides the
                configuration for this felyx instance, felyx will now
                exit. Please read the felyx documentation and ensure that
                your felyx instance is correctly installed.
                """.format(  # noqa: WPS462
                    os.environ[FELYX_CFG_SYS_ENV],
                    FELYX_CFG_SYS_ENV,
                ),
            )

    felyx_config = FelyxReportConfig()
    if configuration_file is not None:
        felyx_config = FelyxReportConfig.from_yaml_file(configuration_file)
    felyx_config.merge(override_data)
    return felyx_config


class FelyxReportSiteCollectionMonitoringConfig(FelyxModel):
    """Felyx report site collection monitoring configuration."""

    alert_threshold_nb_of_in_situ_measurements: int = -1


class FelyxReportSiteCollectionConfig(SiteCollectionConfig):
    """Felyx report site collection configuration."""

    monitoring: FelyxReportSiteCollectionMonitoringConfig = FelyxReportSiteCollectionMonitoringConfig()


class FelyxReportContentSelectionMonitoringConfig(FelyxModel):
    """Felyx report content selection monitoring configuration."""

    alert_threshold_nb_of_created_mmdb_files: int = -1
    alert_check_nb_of_created_matchups: bool = False
    dashboard_create_nb_of_created_matchups: bool = True


class FelyxReportContentSelectionConfig(ContentSelectionConfig):
    """Felyx report content selection configuration."""

    monitoring: FelyxReportContentSelectionMonitoringConfig = FelyxReportContentSelectionMonitoringConfig()


class FelyxReportProductConfig(ProductConfig):
    """Felyx report product configuration."""

    content: Dict[str, FelyxReportContentSelectionConfig]


class FelyxReportMDBOutputConfig(MDBOutputConfig):
    """Felyx report MDB output configuration."""

    products: Union[Dict[str, FelyxReportProductConfig], None] = None


class FelyxReportMetricMonitoringConfig(FelyxModel):
    """Felyx report metric monitoring configuration."""

    alert_check_nb_metrics: bool = False


class FelyxReportMetricConfig(MetricConfig):
    """Felyx report metric configuration."""

    monitoring: FelyxReportMetricMonitoringConfig = FelyxReportMetricMonitoringConfig()


class FelyxReportEODatasetExtractionConfig(EODatasetExtractionConfig):
    """Felyx report EODataset extraction configuration."""

    metrics: Union[Dict[str, FelyxReportMetricConfig], None] = None
    content: FelyxReportContentSelectionConfig = FelyxReportContentSelectionConfig()


class FelyxReportMDatasetConfig(MDatasetConfig):
    """Felyx report MDataset configuration."""

    eo_datasets: Dict[str, FelyxReportEODatasetExtractionConfig]
    output: Union[FelyxReportMDBOutputConfig, None] = None


class FelyxReportDatasetMonitoringConfig(FelyxModel):
    """Felyx report dataset monitoring configuration."""

    alert_check_nb_of_processed_eo_files: bool = False


class FelyxReportDatasetConfig(DatasetConfig):
    """Felyx report dataset configuration."""

    monitoring: FelyxReportDatasetMonitoringConfig = FelyxReportDatasetMonitoringConfig()


class FelyxReportProcessorConfig(FelyxProcessorConfig):
    """Felyx report processor configuration."""

    contact_points: List[ContactPointConfig] = []
    MMDatasets: Dict[str, FelyxReportMDatasetConfig]
    EODatasets: Dict[str, FelyxReportDatasetConfig]
    Metrics: List[FelyxReportMetricConfig] = []
    SiteCollections: Dict[str, FelyxReportSiteCollectionConfig]


def load_felyx_report_processor_config(
    configuration_file: Path = None,
    override_data: Dict = None,
) -> FelyxReportProcessorConfig:
    """Load Felyx report processor configuration from a YAML File.

    Args:
        configuration_file: YAML configuration file
        override_data: override data

    Returns:
        FelyxReportProcessorConfig: an instance of FelyxReportProcessorConfig
    """
    if configuration_file is not None:
        felyx_report_processor_config = FelyxReportProcessorConfig.from_yaml_file(configuration_file)
    else:
        felyx_report_processor_config = FelyxReportProcessorConfig()
    felyx_report_processor_config.merge(override_data)
    return felyx_report_processor_config
