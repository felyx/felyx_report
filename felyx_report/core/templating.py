"""Felyx report templating tool."""

import json
from pathlib import Path
from typing import Dict

from jinja2 import Environment, FileSystemLoader

from felyx_report.core.exceptions import TemplatingError


def render_json_template(template_dir: Path, template_name: str, template_params: Dict) -> str:
    """Render a JSON template using jinja2.

    Args:
        template_dir: The template directory
        template_name: The template file name
        template_params: The template parameters dictionary

    Returns:
        The rendered json template

    Raises:
        TemplatingError: In case of rendering error
    """
    try:
        env = Environment(loader=FileSystemLoader(template_dir))
        env.filters['jsonify'] = json.dumps
        template = env.get_template(template_name)
        return template.render(**template_params)
    except Exception as exception:
        raise TemplatingError('{0}'.format(exception))
