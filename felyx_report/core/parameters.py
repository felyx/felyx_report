"""Felyx report parameters."""

import argparse
from datetime import datetime
from typing import Optional, Union

from pydantic import DirectoryPath

from felyx_processor.utils.configuration import FelyxSystemConfig
from felyx_processor.utils.parameters import FelyxModelParameters


def add_log_params(yaml_data: dict, args: argparse.Namespace):
    """Add the logging parameters retrieved from the command line to the yaml configuration.

    Args:
        yaml_data: The yaml data dictionary
        args: The command line arguments
    """
    yaml_data_log = yaml_data['log']
    yaml_data_log['verbose'] = args.verbose
    yaml_data_log['quiet'] = args.quiet
    yaml_data_log['silent'] = args.silence
    yaml_data_log['logfile'] = args.logfile
    yaml_data_log['logfile_level'] = args.logfile_level


class FelyxReportParams(FelyxModelParameters):
    """Felyx report parameters."""

    start: Union[str, datetime]
    stop: Union[str, datetime]
    manifest_dir: DirectoryPath
    metric_dir: Optional[DirectoryPath]
    mdb_dir: DirectoryPath

    @staticmethod
    def add_arguments(  # noqa: WPS602
        yaml_data: dict,
        args: argparse.Namespace,
        sys_config: FelyxSystemConfig,
    ):
        """Add the felyx report parameters retrieved from the command line to the yaml configuration.

        Args:
            yaml_data: The yaml data dictionary
            args: The command line arguments
            sys_config: The felyx system configuration
        """
        yaml_data['start'] = args.start
        yaml_data['stop'] = args.stop
        yaml_data['manifest_dir'] = args.manifest_dir
        yaml_data['metric_dir'] = args.metric_dir
        yaml_data['mdb_dir'] = args.mdb_dir

        add_log_params(yaml_data, args)
