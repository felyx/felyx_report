"""Felyx report exceptions."""


class FelyxReportError(Exception):
    """Base class for felyx report exceptions."""


class TemplatingError(FelyxReportError):
    """Templating error."""
