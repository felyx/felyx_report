"""Felyx report models."""
import datetime
from typing import Dict, List, Optional

from felyx_processor.utils.configuration import FelyxModel

REPORT_DATE_EXPORT_FORMAT = '%Y-%m-%d %H:%M:%S'  # noqa: WPS323
REPORT_DATE_INDEX_NAME_FORMAT = '%Y%m%d%H%M%S'  # noqa: WPS323


def add_key_prefix(source_dict: dict, prefix: str) -> dict:
    """Add a prefix to each key of a dictionary.

    Args:
        source_dict: The source dictionary
        prefix: The prefix to be added

    Returns:
        The prefixed dictionary
    """
    return {'{0}_{1}'.format(prefix, dict_key): dict_val for dict_key, dict_val in source_dict.items()}


class ProcessedFilesSection(FelyxModel):
    """Processed files section model."""

    nb_processed_eo_files: int
    nb_manifests: int

    def export_model(self) -> dict:
        """Export Processed files section.

        Returns:
            A dictionary representing the Processed files section
        """
        return {
            'nb_eo_files': self.nb_processed_eo_files,
            'nb_manifest_files': self.nb_manifests,
        }


class MetricsSection(FelyxModel):
    """Metrics section model."""

    metrics_data: Dict[str, int]

    def export_model(self) -> dict:
        """Export Metrics section.

        Returns:
            A dictionary representing the Metrics section
        """
        return add_key_prefix(
            source_dict=self.metrics_data,
            prefix='nb_metric',
        )


class MatchUpsSection(FelyxModel):
    """Matchups section model."""

    nb_mmdb_files: Dict[str, int]  # per product
    nb_matchups_manifest: int
    nb_assembled_matchups: Dict[str, int]  # per product

    def export_model(self) -> dict:
        """Export Matchups files section.

        Returns:
            A dictionary representing the Matchups section
        """
        prefixed_nb_mmdb_files_dict = add_key_prefix(
            source_dict=self.nb_mmdb_files,
            prefix='nb_mmdb_files',
        )
        prefixed_nb_assembled_matchups_dict = add_key_prefix(
            source_dict=self.nb_assembled_matchups,
            prefix='nb_assembled_matchups',
        )
        return {
            **prefixed_nb_mmdb_files_dict,
            **{'nb_created_matchups_from_manifest': self.nb_matchups_manifest},
            **prefixed_nb_assembled_matchups_dict,
        }


class EO_source(FelyxModel):  # noqa: N801
    """Earth observations datasource model."""

    name: str
    processed_files: ProcessedFilesSection
    metrics: Optional[MetricsSection]
    matchups: MatchUpsSection

    def export_model(self) -> dict:
        """Export EO_source.

        Returns:
            A dictionary representing the EO_source
        """
        if self.metrics:
            return {
                **self.processed_files.export_model(),
                **self.metrics.export_model(),
                **self.matchups.export_model(),
            }
        return {
            **self.processed_files.export_model(),
            **self.matchups.export_model(),
        }


class MMDBReport(FelyxModel):
    """Multi matchup databases report model."""

    name: str
    report_date: datetime.datetime
    eo_sources: List[EO_source]
    nb_in_situ_measurements: int
    collection_name: str

    def export_model(self, index_prefix: str) -> List[dict]:
        """Export MMDBReport.

        Flatten MMDBReport to create a dictionary per EO source.

        Args:
            index_prefix: The index prefix

        Returns:
            A list of dictionaries representing the flattened MMDBReport
        """
        return [
            {
                **{
                    '_index': '{0}{1}'.format(index_prefix, self.name.lower()),
                    '_id': '{0}_{1}_{2}'.format(
                        self.name,
                        self.report_date.strftime(REPORT_DATE_INDEX_NAME_FORMAT),
                        eo_source.name,
                    ),
                    'mmdb_name': self.name,
                    'report_date': self.report_date.strftime(REPORT_DATE_EXPORT_FORMAT),
                    'eo_source_name': eo_source.name,
                    'nb_in_situ_measurements': self.nb_in_situ_measurements,
                    'collection_name': self.collection_name,
                },
                **eo_source.export_model(),
            } for eo_source in self.eo_sources
        ]
